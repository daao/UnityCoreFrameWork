﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class Build 
{
	[MenuItem("Build/BuildAndroidProject")]
	public static void BuildAndroidProject()
	{
		string AndroidProjectPath = Application.dataPath.Replace("Assets",string.Empty) + "AndroidProject";
		Debug.Log(" AndroidProjectPath " + AndroidProjectPath);
		if(Directory.Exists(AndroidProjectPath)) Directory.Delete(AndroidProjectPath,true);
	
		BuildPipeline.BuildPlayer(
			GetBuildScenes(), 
				AndroidProjectPath, 
					BuildTarget.Android, 
						BuildOptions.AcceptExternalModificationsToPlayer);
	}

	static string[] GetBuildScenes()
	{
		
		List<string> names = new List<string>();
		
		foreach(EditorBuildSettingsScene e in EditorBuildSettings.scenes)
			
		{
			
			if(e==null)
				
				continue;
			
			if(e.enabled)
				
				names.Add(e.path);
			
		}
		
		return names.ToArray();	
	}
	

}
