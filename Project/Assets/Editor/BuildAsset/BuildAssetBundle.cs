﻿using System.IO;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Security.Cryptography;
using System.Text;
using UnityEditor;
using Object = UnityEngine.Object;
using System;
using BlGame.Resource;

//资源assetbundle打包功能
public class BuildAssetBundle
{
    //assetbundle资源
    static string assetFilePath = "/assetbundles";
    //static string buildCfgFilePath = Application.dataPath + "/Editor/Build/build.txt";
    static string levelCfgFilePath = Application.dataPath + "/Editor/Build/level.txt";
    static string buildResourceCfg = Application.dataPath + "/Editor/Build/Resource.txt";
    //static string buildResourceCfg = "Assets/Editor/Build/Resource.txt";

    //打包环境设置	
    static BuildAssetBundleOptions options = 
        BuildAssetBundleOptions.CollectDependencies 
            | BuildAssetBundleOptions.CompleteAssets 
                | BuildAssetBundleOptions.DeterministicAssetBundle
					| BuildAssetBundleOptions.UncompressedAssetBundle;
	static BuildTarget buildPlatform = BuildTarget.StandaloneWindows;

    //保存所有的scene信息
    static List<string> mScenes = new List<string>();

    //保存所有Resource信息
    static List<string> mResources = new List<string>();

    //保存所有的Asset信息 场景+Resource
    static Dictionary<int, Dictionary<string, AssetUnit>> allLevelAssets = new Dictionary<int, Dictionary<string, AssetUnit>>();


    ///不用特意为Editor打包 通用
    
    //Andriod打包
    [MenuItem("Build/BuildAndriod")]
    public static void BuildAndriod()
    {
        buildPlatform = BuildTarget.Android;
        ResourceCommon.buildTarget = "Android";
        BuildResourceFromUnityRule();
    }

    //IOS打包
    [MenuItem("Build/BuildIOS")]
    public static void BuildIOS()
    {
        buildPlatform = BuildTarget.iPhone;
        ResourceCommon.buildTarget = "iPhone";
        BuildResourceFromUnityRule();
    }

    //WindowsPlayer打包
    [MenuItem("Build/BuildWindows also run StandaloneOSX")]
    public static void BuildWindows()
    {
        buildPlatform = BuildTarget.StandaloneWindows;
        ResourceCommon.buildTarget = "StandaloneWindows";
        BuildResourceFromUnityRule();
    }



    //public static VersionResourceFile init_versionResourceFile;
    //public class VersionResourceFile
    //{
    //    public string mNumber;
    //    public bool mBig;
    //    public Dictionary<string, string> mPathMD5;
    //    public Dictionary<string, int> mSize;
    //    public Dictionary<string, int> mVersion;

    //    public VersionResourceFile()
    //    {
    //        mPathMD5 = new Dictionary<string, string>();
    //        mSize = new Dictionary<string, int>();
    //        mVersion = new Dictionary<string, int>();
    //    }
    //}


    //private static void parseResourceVersionFile(string text)
    //{
    //    init_versionResourceFile = new VersionResourceFile();

    //    XmlDocument doc = new XmlDocument();
    //    doc.LoadXml(text);
    //    XmlElement root = doc.DocumentElement;
    //    init_versionResourceFile.mNumber = root.GetAttribute("Number");
    //    init_versionResourceFile.mBig = Convert.ToBoolean(root.GetAttribute("Big"));
    //    IEnumerator iter = root.GetEnumerator();
    //    while (iter.MoveNext())
    //    {
    //        XmlElement child = iter.Current as XmlElement;
    //        init_versionResourceFile.mPathMD5.Add(
    //            child.GetAttribute("fpath"), child.GetAttribute("md5"));
    //        init_versionResourceFile.mSize.Add(
    //            child.GetAttribute("fpath"), Convert.ToInt32(child.GetAttribute("size")));
    //        init_versionResourceFile.mVersion.Add(
    //            child.GetAttribute("fpath"), Convert.ToInt32(child.GetAttribute("version")));
    //    }
    //}

    public static void BuildResourceFromUnityRule()
    {
        //清空数据
        if (Directory.Exists(ResourceCommon.build_assetbundleFilePath))
            Directory.Delete(ResourceCommon.build_assetbundleFilePath, true);

        //刷新数据
        Caching.CleanCache();
        AssetDatabase.Refresh();

        //获取资源信息
        GetBuildScenes();
        //GetBuildResources();
        GetBuildResourcesV2();

        //获取所有的打包asset
        GetAllAssets();

        //保存Asset资源信息
        DumpAssetInfoFile();

        ////打包asset
        BuildResource();

        ////保存资源配置assetbundle
        DumpResourceFile();

        ////根据所有assetbundle文件生成版本信息
        DumpVersionFile();

        ////根据生成的assetbundle文件大小填充 assetInfo.byte文件 
        AddAssetSizeToAssetInfoFile();

        AssetDatabase.Refresh();
        DebugEx.LogError("BuildResource finish", "BuildResource", true);
    }

    //获取需要打包Resources目录下所有资源信息
    //目前不是全部资源打包 只需要考虑局部资源打包
    [MenuItem("Build/Tools/GetBuildResources")]
    public static void GetBuildResourcesV2()
    {
        //mResources.Clear();

        List<string> localPaths = new List<string>();

        //获取资源路径路径
        FileStream fs = new FileStream(buildResourceCfg, FileMode.Open);
        StreamReader sr = new StreamReader(fs);
        string path = "";
        while (path != null) {
            if (path != "")
            {
                //mResources.Add(path);
                localPaths.Add(path);
            }
            path = sr.ReadLine();
        }

        sr.Close();
        fs.Close();
        string[] localPath = localPaths.ToArray();
        foreach (var item in localPath)
        {
            Debug.Log(" item " + item);
        }
        GetBuildResources(localPath);
    }

    public static void GetBuildResources(string[] specific_path)
    {

        //Assets/Resources/Audio/AudioKill/Victory.mp3
        //Assets/Resources/Audio/EnvironAudio/GameOverDefeat.mp3
        mResources.Clear();

        //Resource资源路径
        //string resourcePath = Application.dataPath + "/Resources/";

        foreach (var cur_specific_path in specific_path)
        {
            string resourcePath = Application.dataPath + "/" + cur_specific_path;


			string[] files = null;
			///consider sigle asset not path
			if(Directory.Exists(resourcePath)){
				files = Directory.GetFiles(resourcePath, "*.*", SearchOption.AllDirectories);
			}
			else{
				files = new string[]{ resourcePath };
			}

//            string[] files = Directory.GetFiles(resourcePath, "*.*", SearchOption.AllDirectories);
            foreach (string file in files)
            {
                string suffix = BuildCommon.getFileSuffix(file);
                if (suffix == "meta")
                    continue;

                //Debug.Log(" file " + file);
                string realFile = file.Replace("\\", "/");
                realFile = realFile.Replace(Application.dataPath, "Assets");

                if (realFile == "Assets/Resources/Version.bytes") continue;

                mResources.Add(realFile);
                //Debug.Log(" realFile " + realFile);
            }
        }
    }



    //获取选中需要打包的Resources资源信息
    [MenuItem("Build/Tools/GetSelectedBuildResources")]
    public static void GetSelectedBuildResources()
    {  
        mResources.Clear();

        //Resource资源路径
        //string resourcePath = Application.dataPath + "/Resources/";

        //string[] files = Directory.GetFiles(resourcePath, "*.*", SearchOption.AllDirectories);
        //foreach (string file in files)
        //{
        //    string suffix = BuildCommon.getFileSuffix(file);
        //    if (suffix == "meta" || suffix == "cs")
        //        continue;
        //    string realFile = file.Replace("\\", "/");
        //    realFile = realFile.Replace(Application.dataPath, "Assets");
        //    mResources.Add(realFile);
        //}
        Object[] selection = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);
        if (selection.Length == 0)
            return;

        foreach (Object obj in selection)
        {
            string assetPath = AssetDatabase.GetAssetPath(obj);

            //BuildCommon.getAssetLevel(assetPath);


            Debug.Log("select:" + assetPath);
        }
    }



    //获取需要打包的Scene目录下的所有资源信息
    public static void GetBuildScenes()
    {
        mScenes.Clear();

        //获取场景路径
        FileStream fs = new FileStream(levelCfgFilePath, FileMode.Open);
        StreamReader sr = new StreamReader(fs);
        string path = "";
        while (path != null)
        {
            if (path != "")
            {
                mScenes.Add(path);
            }
            path = sr.ReadLine();
        }
        sr.Close();
        fs.Close();
    }


    [MenuItem("Build/Tools/Test")]
    public static void TestSceneAssets()
    {
        //string path = "Assets/Resources/battle/Model/Hero/1102_火虎.prefab";
        ////string path = "Assets/NGUI/Examples/Models/Orc/FBX.FBX";

        //UnityEngine.Object[] assets =
        //    //new UnityEngine.Object[] {
        //        AssetDatabase.LoadAllAssetsAtPath(path);
        //    //};

        //string[] depends = AssetDatabase.GetDependencies(new string[] { path });
        //foreach (string str in depends)
        //{
        //    Debug.Log("depency: " + str);
        //}

        //List<string> tmp = new List<string>();
        //int index = 0;
        //foreach (Object asset in assets){
        //    Debug.Log("loaded asset " + asset.name + " of type " + asset.GetType());
        //    index++;
        //    tmp.Add(index+"");
        //}
        //UnityEngine.Object mainAsset = assets[0];

        //string savePath =
        //    "f:/assetbundles/windows/"
        //        + "d4fe2a6e186849f4086398d2ceeb0510"
        //            + ResourceCommon.assetbundleFileSuffix;
        ////BuildPipeline.BuildAssetBundle(
        ////    mainAsset, assets, savePath,
        ////        BuildAssetBundleOptions.CollectDependencies
        ////            | BuildAssetBundleOptions.CompleteAssets
        ////                | BuildAssetBundleOptions.DeterministicAssetBundle,
        ////                    BuildTarget.StandaloneWindows); /// 会收集依赖  比如 fbx@idle 等等


        //BuildPipeline.BuildAssetBundleExplicitAssetNames(
        //     assets, tmp.ToArray(), savePath,
        //        BuildAssetBundleOptions.CollectDependencies
        //            | BuildAssetBundleOptions.CompleteAssets
        //                | BuildAssetBundleOptions.DeterministicAssetBundle,
        //                    BuildTarget.StandaloneWindows); ///不会收集依赖  比如 fbx@idle 等等


        string sceneName = "Assets/Scenes/battle/MapEditor/1.unity";

        AssetUnit unit = new AssetUnit(sceneName);

        //string[] depends = AssetDatabase.GetDependencies(new string[] { sceneName });
        //foreach (string str in depends)
        //{
        //    Debug.Log("depency: " + str);
        //}
    }

    //获取所有的Asset
    [MenuItem("Build/Tools/GetAllAssets")]
    public static void GetAllAssets()
    {
        //清空操作
        allLevelAssets.Clear();

        List<string> allAssetPath = new List<string>();
        //1添加场景路径   
       
        foreach (string scene in mScenes)
        {
            allAssetPath.Add(scene);
        }

        //2添加Resource资源路径
        foreach(string resrPath in mResources)
        {
//            Debug.Log("resrPath" + resrPath);
            allAssetPath.Add(resrPath);
        }
       
        //获取场景以及预制件中所有的资源信息
        string[] allExportAssets = AssetDatabase.GetDependencies(allAssetPath.ToArray());


        Dictionary<string, AssetUnit> allFiles = new Dictionary<string, AssetUnit>();
        foreach (string p in allExportAssets)
        {
//            if (p.Contains(".dll") || p.Contains(".cs"))
            if (p.Contains(".dll")) continue;
            //Debug.Log("P" + p);

            AssetUnit unit = new AssetUnit(p);
          
            //Asset等级
            int level = unit.mLevel;

            //存在
            if (allLevelAssets.ContainsKey(level))
            {
                allLevelAssets[level].Add(p, unit);
            }
            else 
            {
                //添加等级
                Dictionary<string, AssetUnit> levelAsset = new Dictionary<string, AssetUnit>();
                allLevelAssets.Add(level,levelAsset);
                //添加asset信息
                allLevelAssets[level].Add(p, unit);
            }
        }

        //创建Asset索引
        //BuildAssetUnitIndex();
    }

    [MenuItem("Build/Tools/CheckResources")]
    public static void CheckResources()
    {
         AssetDatabase.Refresh();

        //获取资源信息
        GetBuildScenes();
        GetBuildResourcesV2();

        CheckAssetValid();

        Debug.Log("Check finised");

        //打包asset
        BuildResource();
    }

   
    //[MenuItem("Build/Tools/BuildSelected")]
    //public static void BuildSelected()
    //{
    //    //清空数据
    //    if (Directory.Exists(ResourceCommon.build_assetbundleFilePath))
    //        Directory.Delete(ResourceCommon.build_assetbundleFilePath, true);

    //    //刷新数据
    //    Caching.CleanCache();
    //    AssetDatabase.Refresh();

    //    //获取资源信息
    //    GetBuildScenes();
    //    GetSelectedBuildResources();

    //    //获取所有的打包asset
    //    GetAllAssets();

    //    //保存Asset资源信息
    //    DumpAssetInfoFile();

    //    //打包asset
    //    BuildResource();

    //    //保存资源配置assetbundle
    //    //DumpResourceFile();

    //    //根据所有assetbundle文件生成版本信息
    //    //DumpVersionFile();

    //    //根据生成的assetbundle文件大小填充 assetInfo.byte文件 
    //    //AddAssetSizeToAssetInfoFile();

    //    AssetDatabase.Refresh();
    //    DebugEx.LogError("BuildResource finish", "BuildResource", true);
    //}



    public static void BuildResource()
    {
        AssetDatabase.Refresh();

        //执行依赖性打包

        //资源最大等级
        int maxLevel = allLevelAssets.Count;
        if (maxLevel == 0)
            return;

        //从最低等级开始打包
        for (int level = 1; level <= maxLevel; level++)
        {
            BuildPipeline.PushAssetDependencies();
       
            //获取不同等级的aaset
            Dictionary<string, AssetUnit> levelAssets = allLevelAssets[level];
           
            //遍历该等级的所有asset打包
            foreach(KeyValuePair<string, AssetUnit> pair in levelAssets)
            {
                //根据路径获取asset资源
                Object asset = AssetDatabase.LoadMainAssetAtPath(pair.Value.mPath);
                
                if (null == asset)
                    DebugEx.LogError("load " + pair.Value.mPath + " failed!!!", "BuildResource", true);


                //生成打包保存路径
                //string savePath = pair.Value.mPath.Insert(6, assetFilePath) + ResourceCommon.assetbundleFileSuffix;
                string savePath =
                    ResourceCommon.build_assetbundleFilePath 
                        //+ pair.Value.mPath.Replace("Assets/", "")
                           + pair.Value.mAssetGUID
                                + ResourceCommon.assetbundleFileSuffix;
                Debug.Log(" savePath " + savePath);
                BuildCommon.CheckFolder(BuildCommon.getPath(savePath));

                //打包名称去Asset
               


                //普通资源
                if (pair.Value.mSuffix != "unity")//普通资源
                {
                    //string assetName = pair.Value.mPath.Replace("Assets/", "");

                    //资源打包
                    if (!BuildPipeline.BuildAssetBundleExplicitAssetNames(
                               new Object[] { asset }, new string[] { pair.Value.mPath }, savePath, options, buildPlatform))
                        DebugEx.LogError("build assetbundle " + savePath + " failed!!!", "BuildResource", true);
                    
                    //Debug.Log("asset path" + pair.Value.mPath);
                }
                //场景资源，没有依赖场景的
                else 
                {
                    AssetDatabase.Refresh();
                    BuildPipeline.PushAssetDependencies();
                    string error = BuildPipeline.BuildStreamedSceneAssetBundle(new string[] { pair.Value.mPath }, savePath, buildPlatform);
                    if (error != "")
                        DebugEx.LogError(error, "BuildResource", true);
                    BuildPipeline.PopAssetDependencies();

                    Debug.Log("scene path" + pair.Value.mPath);
                    //pair.Value.mPath
                }
            }   
        }

        //popdepency依赖
        for (int level = 1; level <= maxLevel; level++)
        {
            BuildPipeline.PopAssetDependencies();
        }
    }


    //创建Asset资源信息
    public static void DumpAssetInfoFile()
    {
        if(allLevelAssets.Count ==0)
            return;

        ////创建所有资源Asset列表
        XmlDocument doc = new XmlDocument();
        XmlElement root = doc.CreateElement("AllAssets");

        //遍历所有Asset数据
        for (int level = 1; level <= allLevelAssets.Count; level++)
        {
            Dictionary<string, AssetUnit> levelAssets = allLevelAssets[level];
            foreach (KeyValuePair<string, AssetUnit> asset in levelAssets)
            {
                string assetName = asset.Key;
                AssetUnit assetUnit = asset.Value;

                XmlElement ele = doc.CreateElement("Asset");
                
                //设置路径名称
                //assetName = assetName.Replace("Assets/", "");
                ele.SetAttribute("name", assetName);
                //Debug.Log("assetName"+assetName);
                //设置asset索引
                ele.SetAttribute("guid", assetUnit.mAssetGUID);
               
                //设置等级
                ele.SetAttribute("level", level.ToString());

                List<AssetUnit> sortDepencys = new List<AssetUnit>();
                //获取AssetUnit所有依赖，并排序
                List<string> depencys = assetUnit.mAllDependencies;
                foreach (string depency in depencys)
                {
                    //Debug.Log("depency"+depency);
                    AssetUnit depencyUnit = GetAssetUnit(depency);
                    sortDepencys.Add(depencyUnit);
                }

                //排序
                sortDepencys.Sort(SortAssetUnit);
                //保存依赖索引
                string depencystr = "";
                for (int i = 0; i < sortDepencys.Count; i++)
                {
                    AssetUnit unit = sortDepencys[i];

                    if (i != sortDepencys.Count - 1)
                        depencystr += unit.mAssetGUID + ",";
                    else
                        depencystr += unit.mAssetGUID;
                }
                ele.SetAttribute("depency", depencystr.ToString());

                root.AppendChild(ele);
            }
        }

        doc.AppendChild(root);
        BuildCommon.CheckFolder(BuildCommon.getPath(ResourceCommon.build_assetbundleFilePath));
        doc.Save(ResourceCommon.build_assetbundleFilePath + "AssetInfo.bytes");

        Debug.Log("CreateAssetInfo success!!!");
    }

    //创建资源索引
    public static void DumpResourceFile()
    {
        ////创建所有资源Asset列表
        XmlDocument doc = new XmlDocument();
        XmlElement root = doc.CreateElement("AllResources");

        XmlElement resource = doc.CreateElement("Resources");
        root.AppendChild(resource);
        foreach (string res in mResources)
        {
            string ex = BuildCommon.getFileSuffix(res);
            //string path = res.Replace("Assets/", "");
            string path = res.Replace("." + ex, "");

            XmlElement ele = doc.CreateElement("file");
            ele.SetAttribute("name", path);
            ele.SetAttribute("type", ex);
            resource.AppendChild(ele);
        }

        //创建所有需要打包的Level列表
        XmlElement sceneRes = doc.CreateElement("Level");
        root.AppendChild(sceneRes);
        foreach (string scene in mScenes)
        {
            XmlElement ele = doc.CreateElement("file");

            //string path = scene.Replace("Assets/", "");
            string path = scene.Replace(".unity", "");

            ele.SetAttribute("name", path);
            ele.SetAttribute("type", "unity");
            sceneRes.AppendChild(ele);
        }
        doc.AppendChild(root);
        BuildCommon.CheckFolder(BuildCommon.getPath(ResourceCommon.build_assetbundleFilePath));
        doc.Save(ResourceCommon.build_assetbundleFilePath + "Resource.bytes");

        Debug.Log("CreateResourceCfg success!!!");
    }


    /// <summary>
    /// 建立apk version 模板
    /// </summary>
    [MenuItem("Build/Tools/Dump_Apk_VersionFile")]
    public static void Dump_Apk_VersionFile() 
    {
        ResourceCommon.buildTarget = "Android";
        BuildCommon.CheckFolder(BuildCommon.getPath(ResourceCommon.build_assetbundleFilePath));

        XmlDocument doc = new XmlDocument();
        XmlElement root = doc.CreateElement("Apk_VersionFile");

		root.SetAttribute("version", "1.1");
        root.SetAttribute("forceUpdate", "true");
        root.SetAttribute("size", "100");
		root.SetAttribute("updateAddress", "file://" + ResourceCommon.build_assetbundleFilePath + "xxjt.apk");
        doc.AppendChild(root);
        doc.Save(ResourceCommon.build_assetbundleFilePath + "Apk_VersionFile.bytes");

		Debug.Log("save" + ResourceCommon.build_assetbundleFilePath + "Apk_VersionFile.bytes");
    }

    [MenuItem("Build/Tools/DumpVersionFile")]
    public static void DumpVersionFile()
    {
        List<string> allFiles = new List<string>();
        BuildCommon.GetFiles(ResourceCommon.build_assetbundleFilePath, allFiles, true);

        XmlDocument doc = new XmlDocument(); 
        XmlElement root = doc.CreateElement("Version");



        //bool hasChange = false;

        //List<string> addFile = new List<string>(); ///该次打包新增的

        root.SetAttribute("Number", "1.0.0");
        root.SetAttribute("Big", "false");
        foreach (string element in allFiles)
        {
            int size = 0;
            string md5 = GetFileMD5(element, ref size);
            XmlElement ele = doc.CreateElement("file");
            //string relativePath = element.Replace(Application.dataPath + "/assetbundles/", "");
            string relativePath = element.Replace(ResourceCommon.build_assetbundleFilePath, "");
            ele.SetAttribute("fpath", relativePath);
            ele.SetAttribute("size", size.ToString());
            ele.SetAttribute("md5", md5);

            //int version = 0;
            /// 如果之前有Version文件
            ///     是否有这个文件的MD5值
            ///        检测MD5值不一样
            ///     检测是否有这个文件
            //if (init_versionResourceFile != null)
            //{
            //    if (init_versionResourceFile.mPathMD5.ContainsKey(relativePath)
            //            && init_versionResourceFile.mPathMD5[relativePath] != md5) {
            //        hasChange = true;
            //    }
            //    else
            //    {
            //        hasChange = true;
            //    }
            //}
            root.AppendChild(ele);

            //保存大小信息到AssetUnit中
            //string assetName = "Assets/" + relativePath;
            //d524ffbf015d7f14ba6ddc35d29f3805.bytes => d524ffbf015d7f14ba6ddc35d29f3805
            string guid = relativePath.Substring(0, relativePath.Length - 6);
            AssetUnit assetUnit = GetAssetUnitByGUID(guid);
            if (assetUnit != null)
                assetUnit.mAssetSize = size;
        }




        doc.AppendChild(root);

        string assetBundleVersionPath = ResourceCommon.build_assetbundleFilePath + "Version.bytes";
        doc.Save(assetBundleVersionPath);

        //拷贝到对应的Resource目录
        //string resourceVersionPath = Application.dataPath + "/Resources/" + "Version.bytes";
        //File.Copy(assetBundleVersionPath, resourceVersionPath, true);

        Debug.Log("Dump Version Success!!!");
    }

    //添加asset信息
    private static void AddAssetSizeToAssetInfoFile()
    {
        //添加asset信息
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(ResourceCommon.build_assetbundleFilePath + "AssetInfo.bytes");

        XmlNodeList xnl = xmlDoc.SelectSingleNode("AllAssets").ChildNodes; 
        for (int i = 0; i < xnl.Count; i++)
        { 
            XmlElement xe = (XmlElement)xnl.Item(i);
            string assetName = xe.GetAttribute("name");

            //获取对应的AssetUnit
            AssetUnit unit = GetAssetUnit(assetName);

            xe.SetAttribute("bundlesize", unit.mAssetSize.ToString());
        }
        xmlDoc.Save(ResourceCommon.build_assetbundleFilePath + "AssetInfo.bytes");
    }

    private static string GetFileMD5(string fpath, ref int size)
    {
        FileStream fs = new FileStream(fpath, FileMode.Open);
        MD5 md5 = MD5.Create();
        byte[] vals = md5.ComputeHash(fs);
        string ret = BitConverter.ToString(vals);
        ret = ret.Replace("-", "");
        size = (int)fs.Length;
        fs.Close();
        return ret;
    }


    //按等级顺序创建AssetUnit的索引信息
    //private static void BuildAssetUnitIndex()
    //{
    //    if (allLevelAssets.Count == 0)
    //        return;

    //    int index = 0;
    //    for (int level = 1; level <= allLevelAssets.Count; level++)
    //    {
    //        Dictionary<string, AssetUnit> levelAssets = allLevelAssets[level];
    //        foreach (KeyValuePair<string, AssetUnit> pair in levelAssets)
    //        {
    //            AssetUnit unit = pair.Value;
    //            unit.mIndex = index;
                
    //            index++;
    //        }
    //    }     
    //}

    public static AssetUnit GetAssetUnitByGUID(string guid) 
    {
        if (allLevelAssets.Count == 0) return null;

        for (int level = 1; level <= allLevelAssets.Count; level++)
        {
            Dictionary<string, AssetUnit> assetUnit = allLevelAssets[level];

            foreach (AssetUnit curAssetUnit in assetUnit.Values)
            {
                if (curAssetUnit.mAssetGUID == guid) return curAssetUnit;
            }
        }
        return null;
    }

    //根据Asset名称获取对应的AssetUnit
    private static AssetUnit GetAssetUnit(string name)
    {
        if (allLevelAssets.Count == 0)
            return null;

        for (int level = 1; level <= allLevelAssets.Count; level++)
        {
            Dictionary<string, AssetUnit> assetUnit = allLevelAssets[level];

            if (assetUnit.ContainsKey(name))
                return assetUnit[name];
        }

        return null;
    }


     //检查所有的打包资源
     private static void CheckAssetValid()
     {
          //清空操作
        allLevelAssets.Clear();

        List<string> allAssetPath = new List<string>();
        //1添加场景路径
       
        foreach (string scene in mScenes)
        {
            allAssetPath.Add(scene);
        }

        //2添加Resource资源路径
        foreach(string resrPath in mResources)
        {
            allAssetPath.Add(resrPath);
        }
       
        //获取场景以及预制件中所有的资源信息
        string[] allExportAssets = AssetDatabase.GetDependencies(allAssetPath.ToArray());

        List<string> checkAssets = new List<string>();

        foreach (string p in allExportAssets)
        {
            if (p.Contains(".dll"))
                continue;

            string assetName = Path.GetFileName(p);
            assetName = assetName.ToLower();

            if(checkAssets.Contains(assetName))
            {
                DebugEx.Log("the asset " + assetName + "has already exsited");
            }
            else
            {
                checkAssets.Add(assetName);
            }
        }

     }

    //对AssetUnit进行排序
    private static int SortAssetUnit(AssetUnit unit1, AssetUnit unit2)
    {
        int res = 0;
        if (unit1.mLevel > unit2.mLevel)
        {
            res = 1;
        }
        else if (unit1.mLevel < unit2.mLevel)
        {
            res = -1;
        }
        return res;
    }
	
}