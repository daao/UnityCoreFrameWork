﻿using UnityEngine;
using System.Collections;

public class GameMain : MonoBehaviour 
{


    public static GameMain Instance;

	// Use this for initialization
	void Start ()  
    {    
        ///整个游戏启动只进行一次
        if (Instance == null) {
            GameStateManager.Instance.EnterDefaultState();

            DontDestroyOnLoad(gameObject);
        }
	}

    void Update() 
    {
        //更新游戏状态机
        GameStateManager.Instance.Update(Time.deltaTime, Time.unscaledDeltaTime);

        //更新UI管理
        SceneMgr.Instance.Update(Time.deltaTime,
            Time.unscaledDeltaTime);
    }

}
