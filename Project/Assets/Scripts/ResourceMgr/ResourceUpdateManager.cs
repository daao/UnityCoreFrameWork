﻿using System;
//using System.Web;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
//using BestHTTP;

namespace BlGame.Resource
{
    //版本信息
    public class VersionFile
    {
        //版本号
        public string version;
        
        //是否强制更新
        public bool forceUpdate;
        
        //版本类型 整包(package)还是资源(resource)
        //public string type;
        
        //大小
        public int size;
        //日期
        public string date;

        
        //更新地址
        public string updateAddress;
    }


    //版本资源信息    
    public class VersionResourceFile
    {
        public string mNumber;
        public bool mBig;
        public Dictionary<string, string> mPathMD5;
        public Dictionary<string, int> mSize;

        public VersionResourceFile()
        {
            mPathMD5 = new Dictionary<string, string>();
            mSize = new Dictionary<string, int>();
        }
    }


    //SDK渠道信息
    class SDKChannelInfo
    {
        //渠道名称
        public string channelName;
        //更新版本地址
        public string updateAddress;
    }


    public class ResourceUpdateManager : UnityNormalSingleton<ResourceUpdateManager>
    {
        //当前版本Version        
        public string mCurVersion;
        //当前版本Identifier
        public string mCurIdentifier;
        //CDN服务器版本信息
        public VersionFile mVersionFile = new VersionFile();
        public VersionResourceFile mLocalPathMD5;
        public VersionResourceFile mServerPathMD5;
        public List<string> mUpdateAssets;
        public List<int> mUpdateAssetsSize;

        //public int apkLoadBytes;
        //public int apkCurLoadBytes;

        public int mAllNeedDownLoadSize = 0;
        public int mCurrentDownLoadSize = 0;
        public int mAllNeedDownLoadBytes = 0;
        public int mCurrentDownLoadBytes = 0;
        public bool mComplete;

        public string mCurrentDownLoadAsset;
        public int mCurrentDownLoadAssetFailedTimes;
        public int mMaxRepeatDownLoadTimes;
        public bool mForceTermination = false;

        public bool mNetworkConnections = true; ///下载资源的连接可能性
        //private bool mNeedCheckNetWorkConnections;
        //private float mCheckNetWorkConnectionsMaxTimes;

        //p Dictionary<string, SDKChannelInfo> mSdkChannelInfos = new Dictionary<string, SDKChannelInfo>();


		string message = "init";
		float apk_progress = 0f;
        void OnGUI()
        {
			GUI.Label(new Rect(10, 0, 20000, 100), message);
			GUI.Label(new Rect(10, 100, 20000, 100), "apk_progress" + apk_progress);
//			GUI.Label(new Rect(10, 200, 20000, 100), (apk_progress == 1.0f ? "finish" : "ready"));
		}

        /***********UI相关**********/

        //提示信息Lable2
//        public ResourceUpdatePanel resourePanel; ///资源更新面板

        private GameObject mProcessBar;
        private GameObject mProcessBar_Lable2;
        private GameObject mProcessBar_BG2;

        //void Start()
        //{
            //this.resourePanel =
                //SceneMgr.Instance.current.panels[PANEL_ID.ResourceUpdate] as ResourceUpdatePanel;

            //base.Awake();

            ////初始更新提示UI
            //mUpdateTipObj = GameObject.Find("TipFrame");
            //if (mUpdateTipObj != null)
            //{

            //    mUpdateTipObj.SetActive(false);

            //    GameObject confirmObj = mUpdateTipObj.transform.FindChild("Confirm").gameObject;
            //    GameObject cancelObj = mUpdateTipObj.transform.FindChild("Cancel").gameObject;

            //    mConfirmButtonPress = confirmObj.GetComponent<ButtonOnPress>();
            //    mCancelmButtonPress = cancelObj.GetComponent<ButtonOnPress>();

            //    //添加listener    
            //    mConfirmButtonPress.AddListener(ConfirmUpdate);
            //    mCancelmButtonPress.AddListener(CancelUpdate);
            //}

            ////进度条
            //mProcessBar = GameObject.Find("ProcessBar");
            //if (mProcessBar != null)
            //{
            //    mProcessBar_BG2 = mProcessBar.transform.FindChild("BG2").gameObject;
            //    mProcessBar_Lable2 = mProcessBar.transform.FindChild("Label2").gameObject;
            //}


            //创建SDK prefab
            //CreateSDKPrefab();
        //}

        public void Start()
        {
            //HTTPManager.MaxConnectionPerServer = 1;
//#if UNITY_STANDALONE_WIN || UNITY_EDITOR || SKIP_SDK
//            OnUpdateComplete();
//#endif
//            this.resourePanel
//                = SceneMgr.Instance.current
//                    .panels[PANEL_ID.ResourceUpdate] as ResourceUpdatePanel;
//            this.resourePanel.ResetLoadProgress();

//            this.mNetworkConnections = true;
        


            updateVersion();
        }



        public bool isUpdateApkState = false;

//        public float preDownLoadBytes = 0f;
//        public float downLoadtimer = 0f;
//        public float detect_time = 0.5f;
        public void Update()
        {
            //显示APK下载进度
            if (this.isUpdateApkState) {
                if (this.apkWWW != null) {
                    
					this.apk_progress = this.apkWWW.progress;
//                    this.resourePanel.ShowDownLoadProgerss(this.apkWWW.progress, true);
//
//                    if ((this.downLoadtimer -= Time.deltaTime) < 0f) {
//                        this.downLoadtimer = detect_time;
//
//                        float speed = (this.apkWWW.bytes.Length - this.preDownLoadBytes);
//                        this.resourePanel.ShowDownLoadSpeed(speed, true);
//
//                        this.preDownLoadBytes = this.apkWWW.bytes.Length;
//                    }
                }
                
            }
//            else {
                //Debug.Log(" this.mCurrentDownLoadBytes " + this.mCurrentDownLoadBytes);
//                this.resourePanel.ShowDownLoadProgerss(
//                    mCurrentDownLoadBytes ==0 && this.mAllNeedDownLoadBytes == 0 ? 
//                        0f : (float)this.mCurrentDownLoadBytes / (float)this.mAllNeedDownLoadBytes, false);
//
//                if ((this.downLoadtimer -= Time.deltaTime) < 0f)
//                {
//                    this.downLoadtimer = detect_time;
//
//                    float speed = (this.mCurrentDownLoadBytes - this.preDownLoadBytes);
//                    this.resourePanel.ShowDownLoadSpeed(speed, true);
//
//                    this.preDownLoadBytes = this.mCurrentDownLoadBytes;
//                }
//            }

//            if (mForceTermination || !mNetworkConnections) StopAllCoroutines();
        }

        //////////////////////////////////////////
		#region update apk
		//强更新，更新版本
		public void updateVersion()
		{
			this.isUpdateApkState = true;
			
			//            this.preDownLoadBytes = 0f;
			//            this.downLoadtimer = 0f;
			
			//            this.resourePanel.ResetLoadProgress();
			
			//读取sdk配置信息
			//loadSdkCfg();
			
			
			//            this.resourePanel.showOperationTipUI("稍等获取当前版本...");
			//
			//            //获取当前版本Version
			this.mCurVersion = SDKConnect.Instance.getVersionName();
			this.message = "当前版本" + this.mCurVersion;
			
			if (this.mCurVersion == "")
			{
				this.message  = "当前版本错误......";
				return;
			}
			//
			//
			//
			this.message  = "稍等,正在检测apk版本更新信息";
			
			//加载版本Version.xml信息
			StartCoroutine(downLoadCheckVersion(ResourceCommon.checkVersionAddress, delegate(WWW serverVersion)
			                                    {
				this.message = "获取服务器apk版本结束,准备解析服务器版本......";
				
				//解析版本信息
				parseVersionFile(serverVersion.text, ref mVersionFile);
				
				//解析版本错误
				if (this.mVersionFile.version == null)
				{
					this.message = "更新版本错误";
					return;
				}
				
				//根据版本情况判断是否更新
				//版本不相同，更新
				if (mVersionFile.version != this.mCurVersion) {
					
					this.message = "版本不同";
					
					///如果是要求强制更新 那就不提示了
					if (mVersionFile.forceUpdate) {
						this.message = "检测到大版本更新 " + mVersionFile.version + ",亲,再不更新就来不及咯!";
						//StartCoroutine(UpdateAPK());
						Update_ThenInstallAPK();
					}
					else {
						this.message = "检测到小版本更新" + mVersionFile.version;
					}
				}
				//版本相同
				else {
					this.message = "和服务器版本一致 直接进行资源更新";
					//updateResource();
					//GameMain.Instance.StartCoroutine();
					updateResource();
				}
			}));
		}
		
		public void Update_ThenInstallAPK()
		{
			StartCoroutine(downLoadAPK(this.mVersionFile.updateAddress, delegate(WWW apkWW)
			                           {
				string apkPath = ReplaceLocalRes("xxju.apk", this.apkWWW.bytes);
				
				SDKConnect.Instance.openFile(apkPath);
			}));
		}
		
		public WWW apkWWW;
		private IEnumerator downLoadAPK(string url, HandleFinishDownload finishFun)
		{
			Debug.Log("downLoadAPK ..." + url);
			
			this.apkWWW = new WWW(url);
			
			yield return this.apkWWW;
			
			Debug.Log("downLoadAPK finish ...");
			
			//网络出错
			if (!string.IsNullOrEmpty(this.apkWWW.error)) {
				Debug.Log(this.apkWWW.error);
				this.mNetworkConnections = false;
				this.OnUpdateError();
			}
			//成功
			else
			{
				if (finishFun != null) finishFun(this.apkWWW);
			}
			
			this.apkWWW.Dispose();
			this.apkWWW = null;
		}
		#endregion
        
        //////////////////////////////////////////

		#region update res
		//热更新，更新资源
		public void updateResource()
		{
			this.isUpdateApkState = false;
			
//			this.preDownLoadBytes = 0f;
//			this.downLoadtimer = 0f;
			
			//yield return new WaitForEndOfFrame();
			//yield return new WaitForEndOfFrame();
			
			mLocalPathMD5 = new VersionResourceFile();
			mServerPathMD5 = new VersionResourceFile();
			mUpdateAssets = new List<string>();
			mUpdateAssetsSize = new List<int>();
			mAllNeedDownLoadSize = 0;
			mCurrentDownLoadSize = 0;
			mAllNeedDownLoadBytes = 0;
			mCurrentDownLoadBytes = 0;
			mComplete = false;
			
			mCurrentDownLoadAsset = "";
			mCurrentDownLoadAssetFailedTimes = 0;
			mMaxRepeatDownLoadTimes = 5;
			mForceTermination = false;
			
			//mNetworkConnections = true;
			//mNeedCheckNetWorkConnections = false;
			//mCheckNetWorkConnectionsMaxTimes = 10;
			
			//1.加载本地资源version.xml信息
			//StreamReader localReader 
			string localReader = ResourcesManager.Open("Version");
			
			if (localReader != null) {
				parseResourceVersionFile(localReader, ref mLocalPathMD5);
			}
			else {
				
				Debugger.Log("亲 你的版本信息全部丢失了  所以 科科 你准备全部重新下资源吧 !");
			}
			
			//2.加载服务器version.xml信息
			StartCoroutine(downLoad(ResourceCommon.getServerPath("Version.bytes"), delegate(WWW resourceVersion)
			{
				parseResourceVersionFile(resourceVersion.text, ref mServerPathMD5);
				
				
				if (needUpdate(mLocalPathMD5, mServerPathMD5, mUpdateAssets, mUpdateAssetsSize))
				{
					DebugManager.Log("need update true");
					
					mAllNeedDownLoadSize = mUpdateAssets.Count;
					
					//                    showOperationTipUI("下载最新资源,更新过程中请不要中断下载,不然还会重新下载一次");
					//4.下载最新资源
					downLoadAssets();
				}
				else
				{
					mComplete = true;
					OnUpdateComplete();
					
				}
			}));
		}
		#endregion



        ////////////// 状态回调 ////////////////////

        /// <summary>
        /// 更新 结束 signal
        /// </summary>
        public Callback<ResourceUpdateManager> updateCompleteSignal;

        //更新完成处理
        private void OnUpdateComplete()
        {
//            this.resourePanel.showOperationTipUI("资源更新结束");

//            if (updateCompleteSignal != null) updateCompleteSignal(this);

            //加载场景
            //ResourcesManager.Instance.loadLevel("Scenes/Pvp_Login", null);
            //ResourceUnit unit = ResourcesManager.Instance.loadImmediate("Guis/UIGameLogin", ResourceType.ASSET);
            //GameObject obj = GameObject.Instantiate(unit.Asset) as GameObject;
        
			ResourcesManager.Instance.UsedAssetBundle = true;
			ResourcesManager.Instance.Init();
			ResourceUnit res =  ResourcesManager.Instance.loadImmediate("Cube",ResourceType.ASSET);
			GameObject go = Instantiate(res.Asset) as GameObject;
		}

        ///当更新过程中碰到错误
        private void OnUpdateError() 
        {
//            this.resourePanel.showOperationTipUI("网络错误,更新中断");
        }

        //////////////////////////////////////////

        //解析版本信息
        private void parseVersionFile(string text, ref VersionFile version)
        {
            
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(text);
            XmlElement root = doc.DocumentElement;
            //获取最近的版本信息 
            version.version = root.GetAttribute("version");
            version.forceUpdate = bool.Parse(root.GetAttribute("forceUpdate"));
            version.size = int.Parse(root.GetAttribute("size"));
            version.updateAddress = root.GetAttribute("updateAddress");

            Debug.Log(" version.version " + version.version
                        + " version.forceUpdate " + version.forceUpdate 
                            + " version.updateAddress " + version.updateAddress);
        }


        //解析版本资源信息
        private void parseResourceVersionFile(string text, ref VersionResourceFile file)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(text);
            XmlElement root = doc.DocumentElement;
            file.mNumber = root.GetAttribute("Number");
            file.mBig = Convert.ToBoolean(root.GetAttribute("Big"));
            IEnumerator iter = root.GetEnumerator();
            while (iter.MoveNext()) {
                XmlElement child = iter.Current as XmlElement;
                //Debug.Log(" fpath " + child.GetAttribute("fpath"));
                file.mPathMD5.Add(child.GetAttribute("fpath"), child.GetAttribute("md5"));
                file.mSize.Add(child.GetAttribute("fpath"), Convert.ToInt32(child.GetAttribute("size")));
            }
            //Debug.Log("paser version fil okk" + text);
        }

        private bool needUpdate(VersionResourceFile local, VersionResourceFile server, List<string> update, List<int> updateSize)
        {
            //后期采用先判定版本号
            foreach (KeyValuePair<string, string> file in server.mPathMD5)
            {
                if (local.mPathMD5.ContainsKey(file.Key))
                {
                    if (local.mPathMD5[file.Key] != file.Value)
                    {
                        update.Add(file.Key);
                        //Debugger.Log(" update " + file.Key + "old key " + local.mPathMD5[file.Key] + "now" + file.Value);
                        mAllNeedDownLoadBytes += server.mSize[file.Key];
                        updateSize.Add(server.mSize[file.Key]);
                    }
                }
                else
                {
                    update.Add(file.Key);
                    //Debugger.Log(" add " + file.Key);
                    mAllNeedDownLoadBytes += server.mSize[file.Key];
                    updateSize.Add(server.mSize[file.Key]);
                }
            }

            //测试
            //foreach (string str in update)
            //{
            //    Debug.Log("need update" + str);
            //}
            return update.Count > 0 ? true : false;
        }

 





        private string ReplaceLocalRes(string file, byte[] data)
        {

            //string localPath;
            ////Andrio跟IOS环境使用沙箱目录
            //if (!Application.isEditor)
            //    //Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            //{
            //    localPath = string.Format("{0}/{1}", Application.persistentDataPath, file);
            //}
            ////Window下使用assetbunlde资源目录
            //else
            //{
            //    localPath = ResourceCommon.assetbundleFilePath + file;
                
            //}
            
            //Debug.Log("Replace" + localPath);
            string localPath = ResourceCommon.getPersistPath(file);
            ResourceCommon.CheckFolder(ResourceCommon.getPath(localPath));

            //using (FileStream stream = new FileStream(localPath, FileMode.OpenOrCreate))
            //{
//				FileStream stream = new FileStream(localPath, FileMode.)
//                stream.Write(data, 0, data.Length);
//                stream.Flush();
//                stream.Close();
            //}

			File.WriteAllBytes(localPath,data);

            return localPath;
        }

        private delegate void HandleFinishDownload(WWW www);
        private delegate void HandleFinishDownloadV2(byte[] www);
        //下载版本检测信息
        private IEnumerator downLoadCheckVersion(string url, HandleFinishDownload finishFun)
        {
            Debug.Log("downLoadCheckVersion ..." + url);
            
            WWW www = new WWW(url);
            
            yield return www;

            Debug.Log("downLoadCheckVersion finish ...");

            //网络出错
            if (!string.IsNullOrEmpty(www.error)) {
                Debug.Log(www.error);
                this.mNetworkConnections = false;
                this.OnUpdateError();
            }
            //成功
            else {
                if (finishFun != null){ finishFun(www); }
            }

            www.Dispose();
            www = null;
        }



        private void downLoadAssets()
        {
            if (0 == mUpdateAssets.Count)
            {
                StartCoroutine(downLoad(ResourceCommon.getServerPath("Version.bytes"), delegate(WWW bytes)
                {
                    //4.覆盖本地version.xml
                    ReplaceLocalRes("Version.bytes", bytes.bytes);
                    OnUpdateComplete();
                    mComplete = true;
                }));
                return;
            }

            string file = mUpdateAssets[0];
            mUpdateAssets.RemoveAt(0);
            ++mCurrentDownLoadSize;
            mCurrentDownLoadBytes += mUpdateAssetsSize[0];
            mUpdateAssetsSize.RemoveAt(0);

            //Http://阿什顿飞打算发的.com ? ask = pathlis = {}
            //WWW www 

            StartCoroutine(downLoad(ResourceCommon.getServerPath(file), delegate(WWW bytes)
            {
                //将下载的资源替换为本地的资源
                ReplaceLocalRes(file, bytes.bytes);
                downLoadAssets();
            }));
        }


        //private IEnumerator downLoad_v2(string url, HandleFinishDownloadV2 finishFun)
        //{
        //    HTTPRequest request = new HTTPRequest(new Uri(url));
        //    request.Send();

        //    yield return StartCoroutine(request);

        //    switch (request.State)
        //    {
        //        case HTTPRequestStates.Finished:
        //            if (request.Response.IsSuccess)
        //            {
        //                finishFun(request.Response.Data);
        //            }
        //            break;
        //        default:
        //            this.mForceTermination = true;
        //            break;
        //    }

        //}

        //下载资源AssetBundle
        private IEnumerator downLoad(string url, HandleFinishDownload finishFun)
        {
            if (mCurrentDownLoadAsset == url)
                ++mCurrentDownLoadAssetFailedTimes;
            else
            {
                mCurrentDownLoadAssetFailedTimes = 0;
                mCurrentDownLoadAsset = url;
            }

            ///重复下载次数过多
            if (mCurrentDownLoadAssetFailedTimes > mMaxRepeatDownLoadTimes)
            {
                this.mForceTermination = true;
                this.OnUpdateError();
                yield break;
            }

            WWW www = new WWW(checkUrl(ref url));
            //Debug.Log(" ready download url " + url);
            
            yield return www;

            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.LogError(www.error);
                //this.OnUpdateError();
                www.Dispose();
                www = null;
                
                //检测网络连接
                //yield return StartCoroutine(checkNetworkConnections());

                StartCoroutine(downLoad(url, finishFun));
            }
            else
            {
                if (finishFun != null)
                {
                    finishFun(www);
                }
                www.Dispose();
                www = null;
            }
        }

        //private IEnumerator checkNetworkConnections()
        //{
        //    mNeedCheckNetWorkConnections = true;
        //    //检测网络
        //    string url = ResourceCommon.getServerPath("checkNetworkConnections.bytes");
        //    float star = System.DateTime.Now.Second;
        //    while (true)
        //    {
        //        WWW www = new WWW(checkUrl(ref url));
        //        yield return www;
        //        if (string.IsNullOrEmpty(www.error))
        //        {
        //            if (www.text == "t")
        //            {
        //                mNeedCheckNetWorkConnections = false;
        //                www.Dispose();
        //                www = null;
        //                break;
        //            }
        //        }
        //        www.Dispose();
        //        www = null;
        //        float end = System.DateTime.Now.Second - star;
        //        if (end > mCheckNetWorkConnectionsMaxTimes)
        //            mNetworkConnections = false;
        //    }
        //}

        private string checkUrl(ref string url)
        {
            string p = url.Replace(" ", "%20");
            //p = p.Replace("+","%2B");
            //p = p.Replace("?","%3F");
            //p = p.Replace("%","%25");
            p = p.Replace("#", "%23");
            //p = p.Replace("&","%26");
            //p = p.Replace("=","%3D");
            return p;
        }
    }


}