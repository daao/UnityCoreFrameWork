﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace BlGame.Resource
{
    public class ResourceAsyncOperation
    {
        public RequestType mRequestType;
        public bool Complete;

        public int mAllDependencesAssetSize;
        public int mLoadDependencesAssetSize;
        public AsyncOperation asyncOperation;
        public ResourceUnit mResource;

        internal ResourceAsyncOperation(RequestType requestType)
        {
            mRequestType = requestType;
            Complete = false;

            mAllDependencesAssetSize = 0;
            mLoadDependencesAssetSize = 0;
            asyncOperation = null;
            mResource = null;    
        }

        //public bool Complete
        //{
        //    get
        //    {
        //        return mComplete;
        //    }
        //}

        //public IEnumerator waitFinish()
        //{
        //    while (!Complete) yield return null;
        //}


        //public Coroutine waitForFinish() 
        //{ 
        //   return GameMain.Instance.StartCoroutine()
        //}

        public int Prograss
        {
            get
            {
                if (Complete)
                    return 100;
                else if (0 == mLoadDependencesAssetSize)
                    return 0;
                else
                {
                    //使用assetbundle
                    //if (ResourcesManager.Instance.UsedAssetBundle)
                    //{
                        if (RequestType.LOADLEVEL == mRequestType)
                        {
                            int depsPrograss = (int)(((float)mLoadDependencesAssetSize / mAllDependencesAssetSize) * 100);
                            int levelPrograss = asyncOperation != null ? (int)((float)asyncOperation.progress * 100.0f) : 0;
                            return (int)(depsPrograss * 0.8) + (int)(levelPrograss * 0.2);
                        }
                        else
                        {
                            return (int)(((float)mLoadDependencesAssetSize / mAllDependencesAssetSize) * 100);
                        }
                    //}
                    ////不使用
                    //else
                    //{
                    //    if (RequestType.LOADLEVEL == mRequestType)
                    //    {
                    //        int levelPrograss = asyncOperation != null ? (int)((float)asyncOperation.progress * 100.0f) : 0;
                    //        return levelPrograss;
                    //    }
                    //    else
                    //    {
                    //        return 0;
                    //    }
                    //}
                }
            }
        }
    }

}