﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace BlGame.Resource
{
    public enum ResourceType
    {
        ASSET,
        //PREFAB,
        //LEVELASSET,
        LEVEL,
		Sprite
    }

    public class ResourceUnit
    {
        public string mPath; ///相对Asset路径
        private Object mAsset;
        private ResourceType mResourceType;
        public List<string> mNextLevelAssets;
        private AssetBundle mAssetBundle;
        public string mAssetBundleName;
        private int mAssetBundleSize;
        private int mReferenceCount;
        //private int mAllDependencesAssetSize;

        internal ResourceUnit(
            string bundleName, AssetBundle assetBundle, int assetBundleSize, 
                Object asset, string path, ResourceType resourceType/*, int allDependencesAssetSize*/)
        {
            mPath = path;
            mAsset = asset;
            mResourceType = resourceType;
            mNextLevelAssets = new List<string>();
            mAssetBundle = assetBundle;
            mAssetBundleName = bundleName;
            mAssetBundleSize = assetBundleSize;
            mReferenceCount = 0;
            //mAllDependencesAssetSize = allDependencesAssetSize;
        }

        public Object Asset
        {
            get
            {
                return mAsset;
            }

            internal set
            {
                mAsset = value;
            }
        }

        public ResourceType resourceType
        {
            get
            {
                return mResourceType;
            }
        }

        //public List<ResourceUnit> NextLevelAssets
        //{
        //    get
        //    {
        //        return mNextLevelAssets;
        //    }

        //    internal set
        //    {
        //        foreach (ResourceUnit asset in value)
        //        {
        //            mNextLevelAssets.Add(asset);
        //        }
        //    }
        //}

        public AssetBundle Assetbundle
        {
            get
            {
                return mAssetBundle;
            }
            set
            {
                mAssetBundle = value;
            }
        }

        public int AssetBundleSize
        {
            get
            {
                return mAssetBundleSize;
            }
        }

        public int ReferenceCount
        {
            get
            {
                return mReferenceCount;
            }
        }

        //public int AllDependencesAssetSize
        //{
        //    get
        //    {
        //        return mAllDependencesAssetSize;
        //    }
        //}

        //public void dumpNextLevel()
        //{
        //    string info = mPath + " the mReferenceCount : " + mReferenceCount + "\n";
        //    foreach (ResourceUnit ru in mNextLevelAssets)
        //    {
        //        ru.dumpNextLevel();
        //        info += ru.mPath + "\n";
        //    }
        //    DebugEx.Log(info, ResourceCommon.DEBUGTYPENAME);
        //}

        //public void addReferenceCount()
        //{
        //    ++mReferenceCount;
        //    foreach (ResourceUnit asset in mNextLevelAssets)
        //    {
        //        asset.addReferenceCount();
        //    }
        //}

        //public void reduceReferenceCount()
        //{
        //    --mReferenceCount;

        //    foreach (ResourceUnit asset in mNextLevelAssets)
        //    {
        //        asset.reduceReferenceCount();
        //    }
        //    if (isCanDestory())
        //    {
        //        //ResourcesManager.Instance.mLoadedResourceUnit.Remove(ResourceCommon.getFileName(mPath, true));
        //        Dispose();
        //    }
        //}

        //public bool isCanDestory() { return (0 == mReferenceCount); }

        public void Dispose()
        {
            ResourceCommon.Log("Destory " + mPath);

            mAsset = null; ///consider Resources.UnloadUnusedAssets

            //if (null != mAssetBundle)
            //{
                mAssetBundle.Unload(true);
                mAssetBundle = null;
            //}
          
        }
    }

    //public class LevelUnit : ResourceUnit
    //{
    //    internal LevelUnit(
    //        string bundleName, AssetBundle assetBundle, int assetBundleSize, 
    //            Object asset, string path, ResourceType resourceType/*, int allDependencesAssetSize*/)
    //        : base(bundleName, assetBundle, assetBundleSize, asset, path, resourceType)
    //    {

    //    }
    //}

}