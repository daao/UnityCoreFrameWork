﻿using System;
using System.IO;
using System.Xml;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;

namespace BlGame.Resource
{
    public class ResourcesManager : UnitySingleton<ResourcesManager>
    {
        //是否通过assetbundle加载资源
        //默认是true
        public bool UsedAssetBundle = true;

        private bool mInit = false;
        //private int mFrameCount = 0;
        private Request mCurrentRequest = null;
        private Queue<Request> mAllRequests = new Queue<Request>();

        //保存读取的Resource信息
        private AssetInfoManager mAssetInfoManager = null;
        //private Dictionary<string, string> mResources = new Dictionary<string, string>();
        
        //加载的资源信息
        private Dictionary<string, ResourceUnit> mLoadedResourceUnit = new Dictionary<string, ResourceUnit>();

        //加载的场景资源
        

        public delegate void HandleFinishLoad(ResourceUnit resource);
        public delegate void HandleFinishLoadLevel();
        public delegate void HandleFinishUnLoadLevel();

        public void Init()
        {
            mAssetInfoManager = new AssetInfoManager();
            mAssetInfoManager.LoadAssetInfo();

            ArchiveManager.Instance.Init();


            mInit = true;
        }

        public void Update()
        {
            if (!mInit)
                return;

            if (null == mCurrentRequest && mAllRequests.Count > 0)
                handleRequest();

            //++mFrameCount;
            //if (mFrameCount == 300)
            //{
            //    //Resources.UnloadUnusedAssets();
            //    mFrameCount = 0;
            //}
        }

        private void handleRequest()
        {
                mCurrentRequest = mAllRequests.Dequeue();

                //相对Asset的完整资源路径
                string fileName = mCurrentRequest.mFileName;

                //ResourceCommon.Log("handleRequest, the type is : " + mCurrentRequest.mResourceType + "\nthe relativePath path is : " + relativePath);

                switch (mCurrentRequest.mRequestType)
                {
                    #region legacy
                    //case RequestType.LOAD:
                    //{
                    //switch (mCurrentRequest.mResourceType)
                    //{
                    //            case ResourceType.ASSET:
                    //            case ResourceType.PREFAB:
                    //                {
                    //                    if (mLoadedResourceUnit.ContainsKey(fileName))
                    //                    {
                    //                        //(mLoadedResourceUnit[fileName] as ResourceUnit).addReferenceCount();

                    //                        mCurrentRequest.mResourceAsyncOperation.mComplete = true;
                    //                        mCurrentRequest.mResourceAsyncOperation.mResource = mLoadedResourceUnit[fileName] as ResourceUnit;

                    //                        if (null != mCurrentRequest.mHandle)
                    //                            mCurrentRequest.mHandle(mLoadedResourceUnit[fileName] as ResourceUnit);
                    //                        handleResponse();
                    //                    }
                    //                    else
                    //                    {
                    //                        //传入相对路径名称
                    //                        //StartCoroutine(_load(fileName, mCurrentRequest.mHandle, mCurrentRequest.mResourceType, mCurrentRequest.mResourceAsyncOperation));
                    //                    }
                    //                }
                    //                break;
                    //            case ResourceType.LEVELASSET:
                    //                {
                    //                    DebugEx.LogError("do you real need a single level asset??? this is have not decide!!!", ResourceCommon.DEBUGTYPENAME);
                    //                }
                    //                break;
                    //            case ResourceType.LEVEL:
                    //                {
                    //                    DebugEx.LogError("this is impossible!!!", ResourceCommon.DEBUGTYPENAME);
                    //                }
                    //                break;
                    //        }
                    //    }
                    //    break;
                    //                    case RequestType.UNLOAD:
                    //                        {
                    //							if (!mLoadedResourceUnit.ContainsKey(fileName)){
                    //								DebugEx.LogError("can not find " + fileName, ResourceCommon.DEBUGTYPENAME);
                    //							}
                    //                            else {
                    //                                //(mLoadedResourceUnit[fileName] as ResourceUnit).reduceReferenceCount();
                    //                            }
                    //                            handleResponse();
                    //                        }
                    //                        break;
                    #endregion

                    case RequestType.LOADLEVEL:
                        {
                            AssetInfo sceneAssetInfo1 = mAssetInfoManager.GetAssetInfo(fileName);
                            string bundleName1 = sceneAssetInfo1.mAssetGUID;
                            if (mLoadedResourceUnit.ContainsKey(bundleName1))
                            {
                                Debugger.LogError("why you load same level twice, maybe you have not unload last time!!!");
                                return;
                            }

                            if (mCurrentRequest.needAsync)
                            {
                                StartCoroutine(
                                    _loadLevel(
                                        mCurrentRequest.mFileName,
                                            mCurrentRequest.mHandleLevel,
                                                ResourceType.LEVEL,
                                                    mCurrentRequest.mResourceAsyncOperation, mCurrentRequest.needAddtive));

                            }
                            else
                            {
                                _loadLevelV2(
                                        mCurrentRequest.mFileName,
                                            mCurrentRequest.mHandleLevel,
                                                ResourceType.LEVEL,
                                                    mCurrentRequest.mResourceAsyncOperation, mCurrentRequest.needAddtive);
                            }
                        }
                        break;
                        
                        #region legacy
                    //case RequestType.UNLOADLEVEL_Or_UNLOADASSET: // 
                    //    {

                    //        AssetInfo sceneAssetInfo = mAssetInfoManager.GetAssetInfo(fileName);
                    //        string bundleName = sceneAssetInfo.mAssetGUID;

                    //        ///consider spawn same hero model but share same hero resource
                    //        if (!mLoadedResourceUnit.ContainsKey(bundleName)) {
                    //            Debug.LogError("can not find unit " + fileName);
                    //        }
                    //        else
                    //        {
                    //            ResourceUnit readyUnloadLevel = mLoadedResourceUnit[bundleName] as ResourceUnit;
                    //            readyUnloadLevel.Dispose();
                    //            mLoadedResourceUnit.Remove(bundleName);

                    //            //if (readyUnloadLevel.mPath == "Assets/Scenes/battle/MapEditor/1.unity")
                    //                //Debug.Log("remove " + readyUnloadLevel.mPath);

                    //            foreach (string curDep in readyUnloadLevel.mNextLevelAssets)
                    //            {
                    //                ResourceUnit tmpAsset = null;
                    //                if (mLoadedResourceUnit.TryGetValue(curDep, out tmpAsset))
                    //                {
                    //                    tmpAsset.Dispose();
                    //                    //Debug.Log(".................. Remove " + curDep);
                    //                    //if (curDep == "b765714cdad342643a93e159d60fa9d6") Debug.Log(" remove " + "b765714cdad342643a93e159d60fa9d6");
                    //                    mLoadedResourceUnit.Remove(curDep); /// may be not cause commmon

                    //                    if (tmpAsset.mPath == "Assets/Scenes/battle/MapEditor/1.unity")
                    //                        Debug.Log("remove " + tmpAsset.mPath);
                    //                }
                    //                //else {
                    //                //    Debug.Log(".................. can not find " + curDep);
                    //                //}
                    //                //ResourceUnit tmpAsset = mLoadedResourceUnit[item.mAssetBundleName] as ResourceUnit;
                    //                //tmpAsset.Dispose();
                    //                //mLoadedResourceUnit.Remove(item.mAssetBundleName); /// may be not cause commmon
                    //            }


                    //            if (mCurrentRequest.mHandleUnloadLevel != null)  mCurrentRequest.mHandleUnloadLevel();
                    //            mCurrentRequest.mResourceAsyncOperation.Complete = true;
                    //        }
                    //        handleResponse();
                    //    }
                    //  break;
                        #endregion

                    default:
                        break;
                }
            
        }

        private void handleResponse()
        {
            mCurrentRequest = null;
        }

        //传入Resources下相对路径名称 例如Resources/Game/Effect1    传入Game/Effect1
		public ResourceUnit loadImmediate(string filePathName, ResourceType resourceType = ResourceType.ASSET,  string archiveName = "Resources")
        {
			string come = "Assets/Resources/" + filePathName + ".prefab";
			AssetInfo asetInfo =  mAssetInfoManager.GetAssetInfo(come);
			List<String> de  = new List<string>();
			foreach (string depence in asetInfo.mDependencys) {
				AssetInfo curde = mAssetInfoManager.GetAssetInfoByGUID(depence);
				_LoadImmediate(curde.mAssetGUID,curde.mName);
			}
			ResourceUnit curUnit = _LoadImmediate(asetInfo.mAssetGUID,asetInfo.mName);
			return curUnit;
        }
        
        public ResourceAsyncOperation loadLevel(
            string fileName, HandleFinishLoadLevel handle, bool needAsync, bool needAddtive, string archiveName = "Level")
        {
            //Assets/Scenes/battle/MapEditor/test
            string completePath = "Assets/" + fileName;
            //获取完整路径
            string completeName = ArchiveManager.Instance.getPath(archiveName, completePath);
            
            ResourceAsyncOperation operation = new ResourceAsyncOperation(RequestType.LOADLEVEL);
            mAllRequests.Enqueue(new Request(completeName, handle, operation, needAsync, needAddtive));
            return operation;
        }

        //no need
        //public void unLoadResource(string fileName, HandleFinishUnLoadLevel handle, string archiveName = "Resources")
        //{
        //    //添加Resource
        //    string completePath = "Assets/Resources/" + fileName;

        //    //Assets/Resources/Game/Effect1.prefab
        //    string completeName = ArchiveManager.Instance.getPath("Resources", completePath);

        //    unLoadResource(completeName);
        //}
        public void unLoadResourceImmediate(string completeName)
        {
            //ResourceAsyncOperation operation = new ResourceAsyncOperation(RequestType.UNLOADLEVEL_Or_UNLOADASSET);
            //mAllRequests.Enqueue(new Request(completeName, null, operation));
            //return operation;

            _unloadRes_or_Level(completeName);
        }

        public void unLoadLevelImmediate(string fileName, HandleFinishUnLoadLevel handle, string archiveName = "Level")
        {
            string completePath = "Assets/" + fileName;
            //获取完整路径
            string completeName = ArchiveManager.Instance.getPath(archiveName, completePath);

            //ResourceAsyncOperation operation = new ResourceAsyncOperation(RequestType.UNLOADLEVEL_Or_UNLOADASSET);
            //mAllRequests.Enqueue(new Request(completeName, handle, operation));
            //return operation;

            _unloadRes_or_Level(completeName);
        }


        public void _unloadRes_or_Level(string completeName)
        {
            AssetInfo sceneAssetInfo = mAssetInfoManager.GetAssetInfo(completeName);
            string bundleName = sceneAssetInfo.mAssetGUID;

            ///consider spawn same hero model but share same hero resource
            if (!mLoadedResourceUnit.ContainsKey(bundleName))
            {
                Debug.LogError("can not find unit " + completeName);
            }
            else
            {
                ResourceUnit readyUnloadLevel = mLoadedResourceUnit[bundleName] as ResourceUnit;
                readyUnloadLevel.Dispose();
                mLoadedResourceUnit.Remove(bundleName);

                //if (readyUnloadLevel.mPath == "Assets/Scenes/battle/MapEditor/1.unity")
                //Debug.Log("remove " + readyUnloadLevel.mPath);

                foreach (string curDep in readyUnloadLevel.mNextLevelAssets)
                {
                    ResourceUnit tmpAsset = null;
                    if (mLoadedResourceUnit.TryGetValue(curDep, out tmpAsset))
                    {
                        tmpAsset.Dispose();
                        //Debug.Log(".................. Remove " + curDep);
                        //if (curDep == "b765714cdad342643a93e159d60fa9d6") Debug.Log(" remove " + "b765714cdad342643a93e159d60fa9d6");
                        mLoadedResourceUnit.Remove(curDep); /// may be not cause commmon

                        //if (tmpAsset.mPath == "Assets/Scenes/battle/MapEditor/1.unity")
                            //Debug.Log("remove " + tmpAsset.mPath);
                    }
                    //else {
                    //    Debug.Log(".................. can not find " + curDep);
                    //}
                    //ResourceUnit tmpAsset = mLoadedResourceUnit[item.mAssetBundleName] as ResourceUnit;
                    //tmpAsset.Dispose();
                    //mLoadedResourceUnit.Remove(item.mAssetBundleName); /// may be not cause commmon
                }
            }
                                
            //if (mCurrentRequest.mHandleUnloadLevel != null)  mCurrentRequest.mHandleUnloadLevel();
            //mCurrentRequest.mResourceAsyncOperation.Complete = true;
        }

        private IEnumerator _loadLevel(string path, HandleFinishLoadLevel handle, ResourceType resourceType, ResourceAsyncOperation operation, bool needAddtive)
        {
                //根据场景名称获取asset信息
                AssetInfo sceneAssetInfo = mAssetInfoManager.GetAssetInfo(path);
                //获取该包总大小
                operation.mAllDependencesAssetSize = mAssetInfoManager.GetAllAssetSize(sceneAssetInfo);

                operation.mResource = new ResourceUnit(
    sceneAssetInfo.mAssetGUID, null, sceneAssetInfo.mSize, null, sceneAssetInfo.mName, ResourceType.LEVEL);
                mLoadedResourceUnit.Add(sceneAssetInfo.mAssetGUID, operation.mResource);
                //获取依赖的asset的索引
                foreach (string index in sceneAssetInfo.mDependencys)
                {
                    //根据索引获取依赖的Asset
                    AssetInfo depencyAsset = mAssetInfoManager.GetAssetInfoByGUID(index);
                    string depencyAssetName = depencyAsset.mName;

                    //加载场景依赖assetbundle
                    ResourceUnit unit = _LoadImmediate(depencyAsset.mAssetGUID, depencyAsset.mName);
                    operation.mResource.mNextLevelAssets.Add(unit.mAssetBundleName);
                    operation.mLoadDependencesAssetSize += unit.AssetBundleSize;
                }

                //加载场景assetbundle     
                int scenAssetBundleSize = 0;
                Debug.Log(" load level " + path);
                byte[] binary = ResourceCommon.getAssetBundleFileBytesV2(sceneAssetInfo.mAssetGUID, ref scenAssetBundleSize);
                AssetBundle assetBundle = AssetBundle.CreateFromMemoryImmediate(binary);
                //AssetBundle assetBundle = ResourceCommon.getAssetBundleFileBytes(sceneAssetInfo.mAssetGUID, ref scenAssetBundleSize);   
                operation.mResource.Assetbundle = assetBundle;
                if (!assetBundle)
                    DebugEx.LogError("create scene assetbundle " + path + "in _LoadImmediate failed");

                //添加场景大小
                operation.mLoadDependencesAssetSize += scenAssetBundleSize;
                AsyncOperation asyncOperation = null;
                if (needAddtive)
                    asyncOperation = Application.LoadLevelAdditiveAsync(ResourceCommon.getFileName(path, false));
                else
                    asyncOperation = Application.LoadLevelAsync(ResourceCommon.getFileName(path, false));
                operation.asyncOperation = asyncOperation;
                yield return asyncOperation;
                operation.Complete = true;
                
                handleResponse();

                //operation.asyncOperation = null;
                //operation.mComplete = true;
                //operation.mResource = null;
;

                if (null != handle)  handle();
        }

        void _loadLevelV2(string path, HandleFinishLoadLevel handle, ResourceType resourceType, ResourceAsyncOperation operation, bool needAddtive) 
        {
                //根据场景名称获取asset信息
                AssetInfo sceneAssetInfo = mAssetInfoManager.GetAssetInfo(path);
                //获取该包总大小
                operation.mAllDependencesAssetSize = mAssetInfoManager.GetAllAssetSize(sceneAssetInfo);

                operation.mResource = new ResourceUnit(
sceneAssetInfo.mAssetGUID, null, sceneAssetInfo.mSize, null, sceneAssetInfo.mName, ResourceType.LEVEL);
                mLoadedResourceUnit.Add(sceneAssetInfo.mAssetGUID, operation.mResource);
                Debug.Log(" mLoadedResourceUnit add" + operation.mResource.mPath);
                //获取依赖的asset的索引
                foreach (string index in sceneAssetInfo.mDependencys)
                {
                    //根据索引获取依赖的Asset
                    AssetInfo depencyAsset = mAssetInfoManager.GetAssetInfoByGUID(index);
                    //string depencyAssetName = depencyAsset.mName;

                    //Debug.Log(" depencyAssetName " + depencyAsset.mName);

                    //加载场景依赖assetbundle
                    ResourceUnit unit = _LoadImmediate(depencyAsset.mAssetGUID, depencyAsset.mName);
                    operation.mResource.mNextLevelAssets.Add(unit.mAssetBundleName);
                    operation.mLoadDependencesAssetSize += unit.AssetBundleSize;
                }

                //加载场景assetbundle     
                int scenAssetBundleSize = 0;
                Debug.Log(" load level " + path);
                byte[] binary = ResourceCommon.getAssetBundleFileBytesV2(sceneAssetInfo.mAssetGUID, ref scenAssetBundleSize);
                AssetBundle assetBundle = AssetBundle.CreateFromMemoryImmediate(binary);
                //AssetBundle assetBundle = ResourceCommon.getAssetBundleFileBytes(sceneAssetInfo.mAssetGUID, ref scenAssetBundleSize);
                operation.mResource.Assetbundle = assetBundle;
                if (!assetBundle)
                    DebugEx.LogError("create scene assetbundle " + path + "in _LoadImmediate failed");

                //添加场景大小
                operation.mLoadDependencesAssetSize += scenAssetBundleSize;

                //AsyncOperation asyncOperation = null;
                if (needAddtive)
                    Application.LoadLevelAdditive(ResourceCommon.getFileName(path, false));
                else
                    Application.LoadLevel(ResourceCommon.getFileName(path, false));
                operation.asyncOperation = null;
                operation.Complete = true;
                
                handleResponse();

                if (null != handle)  handle();
            
        }
        //单个资源加载
		ResourceUnit _LoadImmediate(string assetbundleName, string assetName, ResourceType resourceType = ResourceType.ASSET)
        {
			if(!mLoadedResourceUnit.ContainsKey(assetbundleName))
			{
				int asetsize = 0;
				var ab  = ResourceCommon.getAssetBundleFileBytes(assetbundleName, ref asetsize);
				Object asset = ab.Load(assetName);
				ResourceUnit res = new ResourceUnit(assetbundleName, ab, asetsize,asset,assetName, ResourceType.ASSET);
				return res;
			}
			else{
				return mLoadedResourceUnit[assetbundleName];
			}
        }

        public static string Open(string path)
        {
            //string localPath;
            ////Andrio跟IOS环境使用沙箱目录
            //if (Application.platform == RuntimePlatform.Android 
            //        || Application.platform == RuntimePlatform.IPhonePlayer
            //            || Application.platform == RuntimePlatform.WindowsPlayer)
            //{
            //    localPath = string.Format("{0}/{1}", Application.persistentDataPath, path + ResourceCommon.assetbundleFileSuffix);
            //}
            ////Window下使用assetbunlde资源目录
            //else
            //{
            //    localPath = ResourceCommon.assetbundleFilePath + path + ResourceCommon.assetbundleFileSuffix;
            //}

            string localPath = ResourceCommon.getPersistPath(path + ResourceCommon.assetbundleFileSuffix);
            Debug.Log(" localPath " + localPath);
            //首先检查沙箱目录中是否有更新资源
            if (File.Exists(localPath)) {
                //return File.Open(localPath, FileMode.Open, FileAccess.Read, FileShare.Read);
                return System.IO.File.ReadAllText(localPath, System.Text.Encoding.UTF8);
            }
            else {
                return null;
                //Debug.Log(" Open " + path);
                //TextAsset text = Resources.Load(path) as TextAsset;
                //if (null == text) {
                //    DebugEx.Log(" !!!!!! error can not find : " + path + " in OpenText", ResourceCommon.DEBUGTYPENAME);
                //    return null; ///考虑后期有人破坏整体包数据的情况
                //}
                //else {
                //    using (MemoryStream mem = new MemoryStream(text.bytes)) {
                //        StreamReader reader = new StreamReader(mem, System.Text.Encoding.UTF8);
                //        string result = reader.ReadToEnd();
                //        reader.Close();
                //        return result;
                //    } ;
                //}
            }


            //if (!File.Exists(localPath))//原始包中
            //{
            //    TextAsset text = Resources.Load(path) as TextAsset;
            //    if (null == text)
            //        Debug.LogError("can not find : " + path + " in OpenText", ResourceCommon.DEBUGTYPENAME);
            //    return new MemoryStream(text.bytes);
            //}
            //else//沙箱环境中
            //{
            //    return File.Open(localPath, FileMode.Open, FileAccess.Read, FileShare.Read);
            //}


        }



        //public static string OpenText(string path)
        //{
        //    string result = null;
        //    using (Stream stream =  Open(path))
        //    {
        //        if (stream != null) {
        //            StreamReader reader = new StreamReader(stream, System.Text.Encoding.Default);
        //            //reader.Read(); ///skip BOM  which unity alaways seem to be bom
        //            //StreamReader newreader = XmlReader.Create(reader,XmlReaderSettings);
        //            result = reader.ReadToEnd();
        //            reader.Close();
        //        }
        //        //return
        //        //    stream != null ?
        //        //        new StreamReader(stream, System.Text.Encoding.Default).ReadToEnd() : null;
        //    }
        //    return result;
        //    //Stream stream =  Open(path);


        //}
    }
}