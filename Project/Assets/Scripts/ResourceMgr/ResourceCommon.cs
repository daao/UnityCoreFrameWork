﻿//#define Resource_UseLog

using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;


namespace BlGame.Resource
{
    public class ResourceCommon
    {
        public static string textFilePath = Application.streamingAssetsPath;

        public static string buildTarget;
//		public static string prePath = "f:/assetbundles/111/";
        public static string build_assetbundleFilePath{
            get{
				#if UNITY_EDITOR_WIN
                switch (buildTarget)
                {
                    case "Android": 
						return prePath+"android/";
                    case "iPhone": 
						return prePath+"iphone/";
                    case "StandaloneWindows": 
						return prePath+"windows/";
                    default: 
                        return string.Empty;
                }
#elif UNITY_EDITOR_OSX
//				switch (buildTarget)
//				{
//                    case "Android": return "/Users/zjy/Development/assetbundles/android/";
//                    case "iPhone": return "/Users/zjy/Development/assetbundles/iphone/";
//                    case "StandaloneWindows": return "/Users/zjy/Development/assetbundles/windows/";
//                    default: return string.Empty;
//				}	
				return Application.dataPath.Replace("Assets", "") + "servers/";
#else
                return string.Empty;
#endif
               
            }
        }
        //= "c:/assetbundles/"; ///打包后bunlde的路径





        


        //public static string dataPath
        //{
        //    get
        //    {
        //        if (!Application.isEditor)
        //        {
        //            return Application.persistentDataPath;
        //        }
        //        //Window下使用assetbunlde资源目录
        //        else
        //        {
        //            return ResourceCommon.assetbundleFilePath;
        //        }
        //    }
        //}

        public static string assetbundleFileSuffix = ".bytes";

        public static string DEBUGTYPENAME = "Resource";


        /// APK 版本检测文件地址
        public static string checkVersionAddress =
			@"file://" + build_assetbundleFilePath + "Apk_VersionFile.bytes";
			//@"http://192.168.1.24:8080/Apk_VersionFile.bytes";
            //@"http://www.qw178.com/game/Apk_VersionFile.bytes";



                
		//public static string checkVersionAddress = @"http://120.26.56.18/game/Apk_VersionFile.bytes";
                            //apk update address url @""http://192.168.1.24:8080/test060702.apk

        //assetbundles 
        //test
        //public static string _mServerAssetPath = @"file:///C:/assetbundles/";
        private static string _mServerAssetPath =
			@"file://" + build_assetbundleFilePath;
            //@"http://res.qw178.com/game/assetbundles/";
		//public static string _mServerAssetPath = @"http://120.26.56.18/game/assetbundles/";

        /// <summary>
        /// 服务器资源下载目录 
        /// </summary>
//        public static string mServerAssetPath {
//            get
//            {
////#if UNITY_ANDROID		    
////                return _mServerAssetPath + "android/";
////#elif UNITY_IPHONE		
////                return _mServerAssetPath + "iphone/";
////#elif UNITY_STANDALONE_WIN
////                return _mServerAssetPath + "windows/";
////#elif UNITY_STANDALONE_OSX    
////                return _mServerAssetPath + "osx/";
////#else    
////                return "";
////#endif
//                switch (Application.platform)
//                {
//                    case RuntimePlatform.Android:
//                        return _mServerAssetPath + "android";
//                    case RuntimePlatform.IPhonePlayer:
//                        return _mServerAssetPath + "iphone";
//                    case RuntimePlatform.WindowsPlayer:
//                        return _mServerAssetPath + "windows";
//                    case RuntimePlatform.OSXEditor:
//                    case RuntimePlatform.WindowsEditor:
//						return _mServerAssetPath;
//                    default:
//                        Debug.LogError("this ?!");
//                        return "";
//                }   
//            }
//        }

        public static string getServerPath(string path)
        {
			return string.Format("{0}/{1}", _mServerAssetPath, path);
        }

        ////////////////////////

        public static string assetbundleFilePath = Application.dataPath.Replace("Assets", "") + "assetbundles";
        public static string mPersistAssetPath
        {
            get
            {
                string localPath;
                //Andrio跟IOS环境使用沙箱目录
                if (Application.platform == RuntimePlatform.Android
                        || Application.platform == RuntimePlatform.IPhonePlayer)
                {
                    //localPath = string.Format("{0}/{1}", Application.persistentDataPath, path + ResourceCommon.assetbundleFileSuffix);
                    localPath = Application.persistentDataPath + "/assetbundles";
                }
                //Window下使用assetbunlde资源目录
                else
                {
                    localPath = ResourceCommon.assetbundleFilePath;
                    //+ path + ResourceCommon.assetbundleFileSuffix;
                }
                return localPath;
            }
        }

        public static string getPersistPath(string file)
        {
            return string.Format("{0}/{1}", mPersistAssetPath, file);
        }

        ////////////////

        /// 本地 StreatmAssets 资源目录
        public static string StreamingAssetsPath
        {
            get
            {
                string path = string.Empty;

                switch (Application.platform)
                {
                    case RuntimePlatform.Android:
                        path = "jar:file://" + Application.dataPath + "!/assets/";
                        break;
                    case RuntimePlatform.IPhonePlayer:
                        path = "file://" + Application.dataPath + "/Raw/";
                        break;
                    default:
                        path = "file://" + Application.dataPath + "/StreamingAssets/";
                        break;
                }

                return path;
            }
        }

        public static string getLocalStreamAssetPath(string file)
        {
            //return string.Format("{0}/{1}", mPersistAssetPath, file);
            return StreamingAssetsPath + file;
        }

        ////////////////



        /// <summary>
        /// 是否有本地解压目录
        /// </summary>
        /// <returns></returns>
        //public static bool HasLocalPersistAsset() { return Directory.Exists(mPersistAssetPath); }



        public static void Log(string fileName)
        {
#if Resource_UseLog
            Debug.Log(fileName, DEBUGTYPENAME, true);
#endif
        }

        //根据资源路径获取资源名称
        public static string getResourceName(string resPathName)
        {
            int index = resPathName.LastIndexOf("/");
            if (index == -1)
                return resPathName;
            else
            {
                return resPathName.Substring(index + 1, resPathName.Length - index - 1);
            }
        }


        public static string getFileName(string fileName)
        {
            int index = fileName.IndexOf(".");
            if (-1 == index)
                throw new Exception("can not find .!!!");
            return fileName.Substring(0, index);
        }

        /// a/b/c.unity = > c or c.unity
        public static string getFileName(string filePath, bool suffix)
        {
            if (!suffix)
            {
                string path = filePath.Replace("\\", "/");
                int index = path.LastIndexOf("/");
                if (-1 == index)
                    throw new Exception("can not find .!!!");
                int index2 = path.LastIndexOf(".");
                if (-1 == index2)
                    throw new Exception("can not find /!!!");
                return path.Substring(index + 1, index2 - index - 1);
            }
            else
            {
                string path = filePath.Replace("\\", "/");
                int index = path.LastIndexOf("/");
                if (-1 == index)
                    throw new Exception("can not find /!!!");
                return path.Substring(index + 1, path.Length - index - 1);
            }
        }

        public static string getFolder(string path)
        {
            path = path.Replace("\\", "/");
            int index = path.LastIndexOf("/");
            if (-1 == index)
                throw new Exception("can not find /!!!");
            return path.Substring(index + 1, path.Length - index - 1);
        }

        public static string getFileSuffix(string filePath)
        {
            int index = filePath.LastIndexOf(".");
            if (-1 == index)
                throw new Exception("can not find Suffix!!! the filePath is : " + filePath);
            return filePath.Substring(index + 1, filePath.Length - index - 1);
        }

        public static void getFiles(string path, bool recursion, Dictionary<string, List<string>> allFiles, bool useSuffix, string suffix)
        {
            if (recursion)
            {
                string[] dirs = Directory.GetDirectories(path);
                foreach (string dir in dirs)
                {
                    if (getFolder(dir) == ".svn")
                        continue;
                    getFiles(dir, recursion, allFiles, useSuffix, suffix);
                }
            }

            string[] files = Directory.GetFiles(path);
            foreach (string file in files)
            {
                string fileSuffix = getFileSuffix(file);
                if (fileSuffix == "meta" || fileSuffix == "dll")
                    continue;
                if (useSuffix && fileSuffix != suffix)
                    continue;
                string relativePath = file.Replace("\\", "/");
                relativePath = relativePath.Replace(Application.dataPath, "");
                string fileName = getFileName(file, true);
                if (allFiles.ContainsKey(fileName))
                {
                    allFiles[fileName].Add(relativePath);
                }
                else
                {
                    List<string> list = new List<string>();
                    list.Add(relativePath);
                    allFiles.Add(fileName, list);
                }
            }
        }

        public static void CheckFolder(string path)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        public static string getPath(string filePath)
        {
            string path = filePath.Replace("\\", "/");
            int index = path.LastIndexOf("/");
            if (-1 == index)
                throw new Exception("can not find /!!!");
            return path.Substring(0, index);
        }

        //public static string getLocalPath(string path)
        //{
        //    string localPath = string.Format("{0}/{1}", Application.persistentDataPath, path);
        //    if (!File.Exists(localPath))
        //    {
        //        if (Application.platform == RuntimePlatform.Android)
        //        {
        //            localPath = string.Format("{0}/{1}", Application.streamingAssetsPath, path);
        //        }
        //        else if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
        //        {
        //            localPath = string.Format("file://{0}/{1}", Application.streamingAssetsPath, path);
        //        }
        //        return localPath;
        //    }
        //    return "file:///" + localPath;
        //}

        public static AssetBundle getAssetBundleFileBytes(string path, ref int size)
        {
            string localPath = ResourceCommon.getPersistPath(path + ResourceCommon.assetbundleFileSuffix);
            if (File.Exists(localPath)) {
                return AssetBundle.CreateFromFile(localPath);
            }
            else {
                return null;
            }
        }

        /// <summary>
        /// 专用于场景
        /// </summary>
        /// <param name="path"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static byte[] getAssetBundleFileBytesV2(string path, ref int size)
        {
            //string localPath;

            ////Andrio跟IOS环境使用沙箱目录
            //if (Application.platform == RuntimePlatform.Android
            //     || Application.platform == RuntimePlatform.IPhonePlayer
            //        || Application.platform == RuntimePlatform.WindowsPlayer)
            //{
            //    localPath = string.Format("{0}/{1}", Application.persistentDataPath, path + ResourceCommon.assetbundleFileSuffix);
            //}
            ////Window下使用assetbunlde资源目录
            //else
            //{
            //    localPath = ResourceCommon.assetbundleFilePath + path + ResourceCommon.assetbundleFileSuffix;
            //}

            //Debug.Log(" getAssetBundleFileBytes " + localPath);

            string localPath = ResourceCommon.getPersistPath(path + ResourceCommon.assetbundleFileSuffix);

            //首先检测沙箱目录中是否有更新资源         
            if (File.Exists(localPath))
            {
                return File.ReadAllBytes(localPath);

                //try
                //{

                //    byte[] bytes = null;
                //    using (FileStream bundleFile = File.Open(localPath, FileMode.Open, FileAccess.Read))
                //    {
                //        bytes = new byte[bundleFile.Length];
                //        bundleFile.Read(bytes, 0, (int)bundleFile.Length);
                //        size = (int)bundleFile.Length;
                //    }
                //    return bytes;
                //}
                //catch (Exception e)
                //{
                //    DebugEx.LogError(e.Message, "CreateAssetBunlde", true);
                //    return null;
                //}
            }
            //原始包中
            else
            {
                return null;
                //TextAsset bundleFile = Resources.Load(path) as TextAsset;

                //if (null == bundleFile)
                //    Debug.LogError("load : " + path + " bundleFile error!!!" +  "CreateAssetBunlde");
                //size = bundleFile.bytes.Length;
                //return bundleFile.bytes;
            }
        }
        //public delegate void Handle_CreateFromMemory(AssetBundleCreateRequest request, int size);
        //public static IEnumerator _CreateFromMemory(string path, Handle_CreateFromMemory handle)
        //{
        //    int size = 0;
        //    AssetBundleCreateRequest bundleRequest = AssetBundle.CreateFromMemory(ResourceCommon.getAssetBundleFileBytes(path, ref size));
        //    yield return bundleRequest;
        //    handle(bundleRequest, size);
        //}

        //public delegate void HandleFinishLoadAsyncFromAssetBundle(AssetBundleRequest request);
        //public static IEnumerator LoadAsyncFromAssetBundle(AssetBundle assetbundle, string name, System.Type type, HandleFinishLoadAsyncFromAssetBundle handle)
        //{
        //    AssetBundleRequest request = assetbundle.LoadAsync(name, type);
        //    yield return request;
        //    handle(request);
        //}

        ////仅用于win_editor
        //public static AssetBundle loadAssetBundleImmediateForDebug(string path)
        //{
        //    string localPath = string.Format("file://{0}/{1}", assetbundleFilePath, path + ResourceCommon.assetbundleFileSuffix);
        //    WWW www = new WWW(localPath);

        //    if (!string.IsNullOrEmpty(www.error))
        //        Debug.LogError("the assetbundle path is error : " + path + "\nand the localPath is : " + localPath);

        //    while (true)
        //    {
        //        byte[] b = www.bytes;
        //        if (www.isDone)
        //            break;
        //    }
        //    return www.assetBundle;
        //}
    }

}