﻿using UnityEngine;
using System.Collections;
using BlGame.Resource;
public class simpleload : MonoBehaviour {

	// Use this for initialization
//	void Start () {
//		//我们获取到ab
//		int size = 1;
//		byte[] bytearray = ResourceCommon.getAssetBundleFileBytes("Cube", ref size);
//		AssetBundle ab = AssetBundle.CreateFromMemoryImmediate(bytearray);
//		//ab load 
//		//实例化
//		GameObject asset = ab.Load("Cube",typeof(GameObject)) as GameObject;
//		GameObject cube = Instantiate(asset) as GameObject; 
//	}

	void OnGUI() {
		if (GUI.Button(new Rect(10, 10, 150, 100), "I am a button")) test();
		
	}

	void test()
	{
//		Resources.Load("Cube") as GameObject

		ResourcesManager.Instance.UsedAssetBundle = true;
		ResourcesManager.Instance.Init();
		ResourceUnit res =  ResourcesManager.Instance.loadImmediate("Cube",ResourceType.ASSET);
		GameObject go = Instantiate(res.Asset) as GameObject;
	}
}
