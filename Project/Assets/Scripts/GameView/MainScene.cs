﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class MainScene : BaseScene
{                                                           
    //public static MainScene instance;

    public override void Init(SceneId sceneId, params object[] sceneArgs)
    {
        //instance = this;

        base.Init(sceneId);

        
        PanelUnit mainUI = new MainUI()
        {
            panelId = PANEL_ID.MainUI,
            mResPath = UI.MainUI,
            mResident = false,
            mRoot = null,
            sceneId = this.sceneId,
            sceneObj = this
        };
        this.panels.Add(PANEL_ID.MainUI, mainUI);
    }
}