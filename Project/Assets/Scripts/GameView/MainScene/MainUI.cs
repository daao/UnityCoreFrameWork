﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;
using System.Linq;

using System;
using System.Text;

public class MainUI : PanelUnit
{
    public override void Init()
    {
        Messenger.AddListener(Noticonst.gameState_mainSceneStart.ToString(), Show);
    }

    public override void Release()
    {
        Messenger.RemoveListener(Noticonst.gameState_mainSceneStart.ToString(), Show);
    }

    //float fBuGeitime = 10.0f;  ///聊天按钮
    //float fNonsenseTime = 10.0f; /// 没事做 间隔时间
    //bool isShowNonsense = false; /// 是否显示
    //bool isTaskNosense = false; /// 任务是否已经完成，完成的时候不显示提示
    //DateTime time;
    //CfgConstVo cfgTime1;
    //CfgConstVo cfgTime2;
    //CfgConstVo cfgTimeInterval;
    //public GameObject btnTask; ///任务按钮
    //public GameObject btnTuJian;///图鉴列表                               
    //public GameObject btnEmail;///邮件列表

    //public GameObject chongZhiBtn; ///充值按钮

    //public GameObject RoleheadK;
    //public UI2DSprite spIcon { get { return basePanel.sprite[0]; } }
    //public UI2DSprite spFrame { get { return basePanel.sprite[1]; } }

    //public UILabel name { get { return basePanel.label[0]; } }
    //public UILabel level { get { return basePanel.label[1]; } }
    //public UILabel vipLevel { get { return basePanel.label[2]; } }

    ////public GameObject top_message_panel;

    //ActivityModel.BossActivityType mBossType;

    public override void InitWidget()
    {
        //foreach (var item in this.basePanel.btns)
        //{
        //    UIEventListener.Get(item).onClick += ClickBtn;
        //}
        //fBuGeitime = 0;

    }

    //public NGUITools.LiHuiShowUnit shoUnit = null;  ///当前展示的立绘
    public override void OnUpdate(float deltaTime, float unscaledDeltaTime)
    {
        //base.OnUpdate();

        ////初始化活动界面
        //InitActivity();

        //fBuGeitime -= Time.deltaTime;
        //if (fBuGeitime <= 0)
        //{
        //    //补给站
        //    time = GameTools.GetDateTimeFromClendtTime();
        //    cfgTime1 = StaticDataPool.Instance.configConstPool.GetCfgConstVo(ConstForClient.WEAL_ENERGY_TM_POINT_1);
        //    cfgTime2 = StaticDataPool.Instance.configConstPool.GetCfgConstVo(ConstForClient.WEAL_ENERGY_TM_POINT_2);
        //    cfgTimeInterval = StaticDataPool.Instance.configConstPool.GetCfgConstVo(ConstForClient.WEAL_ENERGY_TM_INTERVAL);
        //    if ((time.Hour >= cfgTime1.value && time.Hour < cfgTime1.value + cfgTimeInterval.value) || (time.Hour >= cfgTime2.value && time.Hour < cfgTime2.value + cfgTimeInterval.value))
        //    {
        //        if (GameTools.GetRubyView(RubyType.Ruby17))
        //        {
        //            this.basePanel.view[2].SetActive(true);
        //        }
        //        else
        //        {
        //            this.basePanel.view[2].SetActive(false);//将补给站红点取消之后再判断其他几个红点是否存在
        //            if (GameTools.GetRubyView(RubyType.Ruby4)
        //                || GameTools.GetRubyView(RubyType.Ruby5)
        //                || GameTools.GetRubyView(RubyType.Ruby6))
        //            {
        //                this.basePanel.view[2].SetActive(true);
        //            }
        //            else
        //            {
        //                this.basePanel.view[2].SetActive(false);
        //            }
        //        }
        //    }
        //    else
        //    {
        //        this.basePanel.view[2].SetActive(false);//将补给站红点取消之后再判断其他几个红点是否存在
        //        if (GameTools.GetRubyView(RubyType.Ruby4)
        //            || GameTools.GetRubyView(RubyType.Ruby5)
        //            || GameTools.GetRubyView(RubyType.Ruby6))
        //        {
        //            this.basePanel.view[2].SetActive(true);
        //        }
        //        else
        //        {
        //            this.basePanel.view[2].SetActive(false);
        //        }

        //    }
        //    fBuGeitime += 10.0f;
        //}

        //updateNonsense();
    }

    public override void OnDisable()
    {
        ///删除顶部消息显示面板
        //NGUITools.Destroy(top_message_panel.gameObject);

        //UILiHuiManager.Instance.RemoveCurShowUnit();
    }

    protected override void OnAddListener()
    {
        //GameMain.Instance.evt.AddListener(Opcodes.RESPONSE_CHANGEAVATAR, UpdateIcon);
        //GameMain.Instance.evt.AddListener(Opcodes.RESPONSE_ROLE_INFO, UpdateIcon);
        //GameMain.Instance.evt.AddListener(Opcodes.RESPONSE_CACHE, UpdateIcon);
        //GameMain.Instance.evt.AddListener(Opcodes.RESPONSE_GETTASKLIST, ResponseTaskList);//获取任务列表
        ////GameMain.Instance.evt.AddListener(NotiConst.Connect, OnConnect);
        ////Messenger.AddListener(NotiConst.gameState_linklchatServer_success.ToString(), gameState_linkchatServer_success);
        ////GameMain.Instance.evt.AddListener(Opcodes.RESPONSE_CHAT, onRESPONSE_CHAT);
        //GameMain.Instance.evt.AddListener(Opcodes.RESPONSE_GETMAILLIST, resp_getMailList);

        //GameMain.Instance.dataPool.roleModel.updateSignal += FreshRoleInfo;
    }
    protected override void OnRemoveListener()
    {
        //GameMain.Instance.evt.RemoveListener(Opcodes.RESPONSE_CHANGEAVATAR, UpdateIcon);
        //GameMain.Instance.evt.RemoveListener(Opcodes.RESPONSE_ROLE_INFO, UpdateIcon);
        //GameMain.Instance.evt.RemoveListener(Opcodes.RESPONSE_CACHE, UpdateIcon);
        //GameMain.Instance.evt.RemoveListener(Opcodes.RESPONSE_GETTASKLIST, ResponseTaskList);//获取任务列表
        ////GameMain.Instance.evt.RemoveListener(NotiConst.Connect, OnConnect);
        ////Messenger.RemoveListener(NotiConst.gameState_linklchatServer_success.ToString(), gameState_linkchatServer_success);
        ////GameMain.Instance.evt.RemoveListener(Opcodes.RESPONSE_CHAT, onRESPONSE_CHAT);
        //GameMain.Instance.evt.RemoveListener(Opcodes.RESPONSE_GETMAILLIST, resp_getMailList);

        //GameMain.Instance.dataPool.roleModel.updateSignal -= FreshRoleInfo;
    }
}
