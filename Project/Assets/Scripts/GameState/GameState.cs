﻿using UnityEngine;
using System.Collections;

public class noneState : FsmState
{
    public override void Reason()
    {
        //throw new System.NotImplementedException();
    }
    public override void Act()
    {
        //throw new System.NotImplementedException();
    }
}

/// <summary>
/// 更新资源
/// </summary>
public class mainState : FsmState
{
    public override void DoBeforeEntering()
    {
        //Messenger.AddListener<string>(NotiConst.gameLevel_trigger_start, OnTriggerStart);
        //Debug.Log(" resourceUpdateState " + " DoBeforeEntering ");

        SceneMgr.Instance.SwitchingScene(SceneId.MainScene);

        Messenger.Broadcast(Noticonst.gameState_mainSceneStart);
    }


    ///// <summary>
    ///// 等到场景加载成功
    ///// </summary>
    ///// <param name="levelName"></param>
    //public void OnTriggerStart(string levelName)
    //{

    //    //  => 平台UI预加载
    //    SceneMgr.Instance.SwitchingScene(SceneId.ReadyLinkPtScene);
    //    Debug.Log("场景 UI 加载");

    //    ///显示Loading Panel
    //    Messenger.Broadcast(NotiConst.gameState_start_res_update);

    //    ///资源解压缩
    //    ResourceExtracManager.Instance.enabled = true;
    //    ResourceExtracManager.Instance.extractFinishSignal += OnExtarctComplete;
    //    ResourceExtracManager.Instance.mStart();
    //}

    //public void OnExtarctComplete(ResourceExtracManager extrackMgr)
    //{
    //    ResourceExtracManager.Instance.enabled = false;
    //    ResourceExtracManager.Instance.extractFinishSignal -= OnExtarctComplete;

    //    ///资源更新
    //    ResourceUpdateManagerV2.Instance.enabled = true;
    //    ResourceUpdateManagerV2.Instance.updateCompleteSignal += OnUpdateComplete;
    //    ResourceUpdateManagerV2.Instance.mStart();
    //}

    //public void OnUpdateComplete(ResourceUpdateManagerV2 resMgr)
    //{
    //    ResourceUpdateManagerV2.Instance.enabled = false;
    //    ResourceUpdateManagerV2.Instance.updateCompleteSignal -= OnUpdateComplete;

    //    //=> 表加载　ResourceMgr init
    //    //ResourcesManager.Instance.UsedAssetBundle = true;
    //    ResourcesManager.Instance.Init();
    //    Debug.Log("ResourceMgr init");

    //    //DataPool.Instance.Release();
    //    DataPool.Instance.mStart();
    //    GameMain.Instance.dataPool = DataPool.Instance;
    //    Debug.Log("刷新动态缓存");

    //    StaticDataPool.Instance.mStart();
    //    GameMain.Instance.staticDataPool = StaticDataPool.Instance; ///加载静态表配置
    //    Debug.Log("加载静态表配置");

    //    //ResourcesManager.Instance.loadLevel("Scenes/battle/MapEditor/12", delegate()
    //    //        {
    //    //            Debugger.Log("load finished");
    //    //}, false, true);



    //    //		GameObject prefab = Util.FightSceneLogic.LoadAsset<GameObject>(
    //    //			ResourcePath.heroModel_Prefab_Path + "/"
    //    //			+ heroId + "_" + heroName);
    //    //		return UnityEngine.Object.Instantiate(prefab) as GameObject;


    //    //		string model_path = ResourcePath.heroModel_Prefab_Path + "/" + 1101 + "_" + "改良型老虎";
    //    //		ResourceUnit resUnit = ResourcesManager.Instance.loadImmediate(model_path, BlGame.Resource.ResourceType.ASSET);
    //    //		GameObject curHero = UnityEngine.Object.Instantiate(resUnit.Asset) as GameObject;

    //    //		string model_path = ResourcePath.heroModel_Prefab_Path + "/" + 1101 + "_" + "改良型老虎";
    //    //		ResourceUnit resUnit = ResourcesManager.Instance.loadImmediate(model_path, BlGame.Resource.ResourceType.ASSET);
    //    //		GameObject curHero = UnityEngine.Object.Instantiate(resUnit.Asset) as GameObject;


    //    GameStateManager.Instance.SwitchState(GameStateManager.LevelState.readyLinkPt);
    //}

    public override void Reason()
    {
        //throw new System.NotImplementedException();
    }
    public override void Act()
    {
        //throw new System.NotImplementedException();
    }

    public override void DoBeforeLeaving()
    {
    }
}

public class startState : FsmState
{
    public override void Reason()
    {
        //throw new System.NotImplementedException();
    }
    public override void Act()
    {
        //throw new System.NotImplementedException();
    }
}