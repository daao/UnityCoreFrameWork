﻿using UnityEngine;
using System.Collections;



public class GameStateManager : Singleton<GameStateManager>
{

	public enum LevelState
	{
		none,
        main, //关卡选择界面
        start //关卡开始界面
	}

    public FsmMgr fsm;
    public GameStateManager()
    {
        fsm = new FsmMgr();
        fsm.AddInitState(new noneState() { stateName = LevelState.none.ToString() });
        fsm.AddState(new mainState() { stateName = LevelState.main.ToString() });
        fsm.AddState(new startState() { stateName = LevelState.start.ToString() });
    }
    public void EnterDefaultState()
    {
        SwitchState(LevelState.main);
    }
    public void SwitchState(LevelState newState, params object[] args) {

        ////Debug.Log(" fsm.curState.stateName " + fsm.curState.stateName);
        ////FsmState preState = fsm.curState;
        ////LevelState preState = LevelState.none;
        ////if (fsm.curState.stateName != LevelState.none.ToString()) 
        //this.preState = (LevelState)System.Enum.Parse(typeof(LevelState), fsm.curState.stateName); 

        fsm.SwitchState(newState);


        ///有些 游戏状态切换 涉及到场景切换
        //switch (newState) 
        //{
        //    case LevelState.resourceUpdate:
        //        LoadManager.LoadSceen(SceneName.PlatformScene);
        //        break;
        //    case LevelState.gamsStart:
        //        ///从战斗中切回主场景要考虑恢复主场景的UI
        //        MainScene_gamsStartState.ResumeMoudle resumeModule;
        //        if (args != null && args.Length > 0){
        //            resumeModule = (MainScene_gamsStartState.ResumeMoudle)args[0];
        //        }
        //        else {
        //            resumeModule = MainScene_gamsStartState.ResumeMoudle.none;
        //        }
				
        //        (fsm.curState as MainScene_gamsStartState).resumeModule = resumeModule;
        //        //if (preState == LevelState.fightStart || preState == LevelState.newBattleGuidefightStart || preState == LevelState.MorraStart  ){

  
        //        //
        //            ///清空战斗场景的ab
        //            //LoadManager.on_preLoadScene_signal += on_preLoadScene_from_fightStart_or_newBattelGuide_fightStart_to_gameStart;
        //        //}
        //        LoadManager.LoadSceen(SceneName.MainScene);
        //        break;
        //    case LevelState.fightStart:
        //        LoadManager.LoadSceen(SceneName.BattleScene);
        //        break;
        //    case LevelState.newBattleGuidefightStart:
        //        LoadManager.LoadSceen(SceneName.NewBattleScene);  ///新手引导战斗
        //        break;
        //    case LevelState.BossStart:
        //        LoadManager.LoadSceen(SceneName.BossScene);
        //        break;
        //    default:
        //        break;
        //}
    }

    public void Update(float deltaTime, float unscaledDeltaTime)  
    {
        this.fsm.Update();
    }
}
