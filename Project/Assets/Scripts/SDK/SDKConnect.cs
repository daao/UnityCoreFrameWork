﻿using UnityEngine;
using System.Collections;

/// <summary>
/// now only condiser android
/// </summary>
public class SDKConnect : UnitySingleton<SDKConnect> {


	//是否初始化成功
//	public static bool isInitSuccess = false;
//	public static bool isLogOut = false;
//	public static bool islink = false;
//	ublic static SdkConector Instance = null;
	
	//Android平台sdk接口
#if UNITY_ANDROID
	public AndroidJavaObject jo = null;
#endif
	void Awake() {
//		Instance = this;
//		DontDestroyOnLoad(gameObject);

		this.Init();
	}

    //instacll apk
    public void openFile(string filePath) {
#if UNITY_EDITOR
        Debug.Log("do nothing");
#elif UNITY_ANDROID
        SDKConnect.Instance.jo.Call("openFile", filePath);
#else
		Debug.Log("do nothing");
#endif
    }

    public string getVersionName()
    {
#if UNITY_EDITOR 
        return "1.1";
#elif UNITY_ANDROID
		return SDKConnect.Instance.jo.Call<string>("getVersionName");
#elif UNITY_IPHONE
		return LsGetBundleVersion();
#else
		return "1.1";
#endif
    }

	public void Init()
	{
		//sdk初始化
		
//		isLogOut = false;
		//测试
#if UNITY_EDITOR 
		Debugger.Log("Sdk Connector start");
#elif UNITY_ANDROID
		AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
#elif UNITY_IPHONE
		LsSetUnityReceiver("SdkConnect");             
		LsNdInit();
#else
		Debugger.Log("Sdk Connector start");
#endif
	}
}
