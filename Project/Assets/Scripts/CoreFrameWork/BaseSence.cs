﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BaseScene : MonoBehaviour
{
    public SceneId sceneId;

    public Dictionary<PANEL_ID, PanelUnit> panels = new Dictionary<PANEL_ID, PanelUnit>();

    public virtual void Init(SceneId sceneId, params object[] sceneArgs)
    {
        this.sceneId = sceneId;
    }

    /// <summary>
    /// add all Panel
    /// 监听错误处理
    /// </summary>
    public virtual void InitScene()
    {
        foreach (PANEL_ID panelId in panels.Keys)
        {
            PanelUnit bPanel = panels[panelId];
            bPanel.Init();
            if (bPanel.mResident)
            {
                bPanel.PreLoad();
            }
        }
    }

    public virtual void ReleaseScene()
    {
        foreach (PANEL_ID panelId in panels.Keys)
        {
            PanelUnit bPanel = panels[panelId];

            bPanel.Hide();
            if (bPanel.mResident) bPanel.DelayDestroy();

            bPanel.Release();
        }

        panels.Clear();
    }

    public virtual void mUpdate(float deltaTime, float unscaledDeltaTime)
    {
        foreach (PanelUnit pWindow in panels.Values)
        {
            if (pWindow.isVisable)
            {
                pWindow.OnUpdate(deltaTime, unscaledDeltaTime);
            }
        }
    }
}


