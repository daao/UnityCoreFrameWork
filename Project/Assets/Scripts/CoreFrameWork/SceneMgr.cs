﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SceneMgr : Singleton<SceneMgr>
{
        public GameObject root = null;

        public BaseScene current;

        public Dictionary<SceneId, BaseScene> scenes = new Dictionary<SceneId, BaseScene>();

        public Callback<SceneId> onShowSceneSignal;
        
        public GameObject top_message_panel = null;

        /// <summary>
        /// 添加顶部跑马灯
        /// </summary>
        //public void AddTopMessage()
        //{
        //    if (SceneMgr.Instance.top_message_panel == null)
        //    {
        //        SceneMgr.Instance.top_message_panel = NGUITools.AddChildV2(
        //            SceneMgr.Instance.current.gameObject,
        //            UI.systemPanelPath + "/" + UI.announcement
        //        );
        //    }   
        //}

        public SceneMgr()
        {
            //Messenger.AddListener<bool>(NotiConst.RequestNetResponse.ToString(), OnWaitingForNet);
            //GameMain.Instance.evt.AddListener(NotiConst.ConnectError, OnConnectError);
            //GameMain.Instance.evt.AddListener(NotiConst.NetError, OnNetError);

            //RoleModel.Instance.levelReallyChangeSignal += OnLevelChanged;
            //RoleModel.Instance.energyChangeSignal += OnEnergyChanged;
        }    

        public void SwitchingScene(SceneId senceId, params object[] sceneArgs)
        {
            //if (current != null && senceId == current.sceneId)
            //{
            //    Debugger.Log("  试图切换场景至当前场景：" + current.sceneId.ToString());
            //    return;
            //}

            HideCurrentScene();

            ShowScene(senceId, sceneArgs);
        }

        private void ShowScene(SceneId sceneId, params object[] sceneArgs)
        {
            GameObject go = new GameObject(sceneId.ToString());

            Transform t = go.transform;
            //t.parent = (root == null ? UIRoot.list[0].transform : root.transform);
            t.parent =
                //GameMain.Instance._2DUI.transform;
                GameObject.Find(UI._2DUIPath).transform;
            t.localPosition = Vector3.zero;
            t.localRotation = Quaternion.identity;
            t.localScale = Vector3.one;
            //go.layer = (root == null ? UIRoot.list[0].gameObject.layer : root.layer);
            go.layer = 
                //GameMain.Instance._2DUI.layer;
                GameObject.Find("2DUI").layer;
            current = go.AddComponent(sceneId.ToString()) as BaseScene; //sceneType.tostring等于该场景的classname
            current.Init(sceneId, sceneArgs);
            current.InitScene();

            scenes.Add(current.sceneId, current);

            if (onShowSceneSignal != null) onShowSceneSignal(sceneId);
            
        }

        public void HideCurrentScene()
        {
            if (current != null)
            {
                current.ReleaseScene();
                
                UnityEngine.Object.Destroy(current.gameObject);

                scenes.Remove(current.sceneId);
            }
        }

        /// <summary>
        /// 当网络延迟的时候
        /// </summary>
        //public GameObject waiting = null; ///网络延迟 显示面板
        //public void OnWaitingForNet(bool isWait)
        //{
        //    //Debug.Log(" OnWaitingForNet " + isWait);

        //    if (isWait && waiting == null)
        //    {
        //        string path = UI.systemCommonPanelPath + "/" + PANEL_ID.common_WaitingForNetResponse.ToString();
        //        GameObject prefab = NGUITools.LoadAsset<GameObject>(path);
        //        this.waiting = NGUITools.AddChild(current.gameObject, prefab);
        //    }
        //    else if(!isWait && waiting != null)
        //    {
        //        NGUITools.Destroy(waiting);
        //        this.waiting = null;
        //    }
        //}

        //public void OnConnectError(object arg)
        //{
        //    NetType netType = (NetType)arg;
        //    ShowReconnectView(netType);
        //}
        //public void OnNetError(object arg)
        //{
        //    NetType netType = (NetType)arg;
        //    ShowReconnectView(netType);
        //}

        //public void ShowReconnectView(NetType netType)
        //{
        //    string path = UI.systemCommonPanelPath + "/" + PANEL_ID.common_reconnect.ToString();
        //    GameObject prefab = NGUITools.LoadAsset<GameObject>(path);
        //    GameObject ready_reconnect = NGUITools.AddChild(current.gameObject, prefab);
        //    ready_reconnect.GetComponent<common_reconnect>().netType = netType;
        //}

        ////public void OnLevelChanged(int oldLevel, int newLevel) {
        ////    if (rolePanel == null) CreateRoleChangeTipPanel();
        ////    rolePanel.GetComponent<RoleLevelUpTipPanel>().InitView(oldLevel, newLevel);
        ////}
        ////public void OnEnergyChanged(int oldValue, int newValue) {
        ////    if (rolePanel == null) CreateRoleChangeTipPanel();
        ////    rolePanel.GetComponent<RoleLevelUpTipPanel>().InitEnergy(oldValue, newValue);
        ////}

        ///// <summary>
        ///// show get soul
        ///// </summary>
        //public void ShowGetSoul(int soulId, Callback destorySignal = null){
        //    //SceneMgr.Instance.ShowGetSoul

        //    Debug.Log(" ShowGetSoul soulId " + soulId);

        //    ConfigHeroVo staticHeroVo = 
        //        GameMain.Instance.staticDataPool.configHeroPool.GetConfigHeroVo(soulId);
        //    string path =
        //        UI.systemCommonPanelPath + "/" + UI.GetSoulPanel + "_" + staticHeroVo.quality;
        //    GameObject goObj = 
        //        UnityEngine.Object.Instantiate(Resources.Load(path)) as GameObject;
			
        //    goObj.transform.parent = current.transform;
        //    goObj.transform.localPosition = Vector3.zero;
        //    goObj.transform.localScale = Vector3.one;

        //    goObj.GetComponent<GetSoulPanel>().Show(soulId, destorySignal);
        //}



        //public void CreateCommandPanel(CommonReward reward)
        //{
        //    string path = UI.systemCommonPanelPath + "/" + UI.RewardPanel;
        //    GameObject goObj = UnityEngine.Object.Instantiate(
        //        Resources.Load(path)) as GameObject;

            
        //    goObj.transform.parent = current.transform;
        //    goObj.transform.localPosition = Vector3.zero;
        //    goObj.transform.localScale = Vector3.one;

        //    goObj.GetComponent<mRewardTipPanelV2>().InitModel(new object[] { reward });
        //}

        //public GameObject CreateConfirmPanel(string desc, CallBackFunctionWithObject OK = null, object paramOK = null, CallBackFunctionWithObject Cancel = null, object paramCancel = null, int type = 0)//ttype =0 普通框 点确定会消失 1 激活码 2 普通框 点确定不消失 有1秒响应间隔
        //{
        //    GameObject goObj;
        //    if (type == 1)
        //    {
        //        goObj = UnityEngine.Object.Instantiate(Resources.Load("UI/LoginPanel/jihuoma")) as GameObject;
        //        goObj.GetComponent<ConfirmPanel>().createJHMId();
        //        //goObj.transform.parent = current.transform;
        //        //goObj.transform.localPosition = Vector3.zero;
        //        //goObj.transform.localScale = Vector3.one;

        //        //goObj.GetComponent<ConfirmPanel>().Create(desc, OK, label.text, Cancel, paramCancel);
        //    }
        //    else
        //    {
        //        goObj = UnityEngine.Object.Instantiate(Resources.Load("UI/PublicPanel/ConfirmPanelV2")) as GameObject;
        //    }
        //    goObj.transform.parent = current.transform;
        //    goObj.transform.localPosition = Vector3.zero;
        //    goObj.transform.localScale = Vector3.one;

        //    goObj.GetComponent<ConfirmPanel>().Create(desc, OK, paramOK, Cancel, paramCancel, type);

        //    return goObj;
        //}
        ////public void CreateLoginPanel()
        ////{
        ////    GameObject goObj = UnityEngine.Object.Instantiate(Resources.Load("UI/Login/registerPanel")) as GameObject;
		
        ////    goObj.transform.parent = current.transform;
        ////    goObj.transform.localPosition = Vector3.zero;
        ////    goObj.transform.localScale = Vector3.one;
		
        ////    goObj.GetComponent<registerPanel>().Create();
        ////}
        //public void CreateItemTipPanel(string nameLab,string typeLab,string miaoShuLab,string fujiaLab,int id,int quality, int itemOrShipSkill = 0)
        //{
        //    GameObject goObj = UnityEngine.Object.Instantiate(Resources.Load("UI/DragItem/Public/tipsPanel")) as GameObject;
		
        //    goObj.transform.parent = current.transform;
        //    goObj.transform.localPosition = Vector3.zero;
        //    goObj.transform.localScale = Vector3.one;
		
        //    goObj.GetComponent<itemTipPanel>().Create(nameLab,typeLab,miaoShuLab,fujiaLab,id,quality, itemOrShipSkill);
        //}

        //public GameObject quitConfirmPanel = null;

        //public void CreateQuitConfirmPanel(CallBackFunctionWithObject OK = null, object paramOK = null, CallBackFunctionWithObject Cancel = null, object paramCancel = null)
        //{
        //    Debug.Log(" CreateQuitConfirmPanel ");

        //    if (this.quitConfirmPanel != null)
        //    {
        //        NGUITools.Destroy(this.quitConfirmPanel.gameObject);
        //        this.quitConfirmPanel = null;
        //    }

        //    this.quitConfirmPanel = UnityEngine.Object.Instantiate(Resources.Load("UI/PublicPanel/common_readyQuit")) as GameObject;
        //    this.quitConfirmPanel.transform.parent = current.transform;
        //    this.quitConfirmPanel.transform.localPosition = Vector3.zero;
        //    this.quitConfirmPanel.transform.localScale = Vector3.one;
        //    this.quitConfirmPanel.GetComponent<ConfirmPanel>().Create("你确定要残忍的退出我吗 ? o(一︿一+)o", OK, paramOK, Cancel, paramCancel);
        //}


        public void CreateTip(string param, float time = 0,bool bPlayEff = false)
        {
            //TipsPanel tip = current.transform.GetComponentInChildren<TipsPanel>();
            //if (tip != null)
            //{
            //    tip.DestroyPanel(null);
            //}
            //GameObject goObj = UnityEngine.Object.Instantiate(Resources.Load("UI/PublicPanel/TipsPanel")) as GameObject;

            //goObj.transform.parent = current.transform;
            //goObj.transform.localPosition = Vector3.zero;
            //goObj.transform.localScale = Vector3.one;

            //goObj.GetComponent<TipsPanel>().Create(param,time);
            //if (bPlayEff)//在金币购买界面播放暴击动画
            //    goObj.transform.Find("jinbibaojiEffects").gameObject.SetActive(true);
        }


        public void Update(float deltaTime, float unscaledDeltaTime)
        {
            ///小红点管理器更新
            //RubyManager.Instance.Update(deltaTime);

            current.mUpdate(deltaTime, unscaledDeltaTime);
        }
        
}


