﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


//[System.Serializable]
public abstract class FsmState
{

    //public string frontState;

    public string stateName;

    //public string nextState;

    public virtual void DoBeforeEntering() { }

    public virtual void DoBeforeLeaving() { }

    public virtual void DrawVisualization() { }

    public abstract void Reason();

    public abstract void Act();
}
    
//[System.Serializable]
public class FsmMgr
{
    public List<FsmState> states = new List<FsmState>();

    public FsmState curState = null;

    public string curStateName = string.Empty;

    public void AddInitState(FsmState s)
    {
        this.curState = s;
        this.curStateName = s.stateName;
        states.Add(s);
    }

    public void AddState(FsmState s)
    {
        states.Add(s);
    }


    public void Update() 
    {
        curState.Reason();
        curState.Act();
    }


    public void SwitchState(Enum target) 
    {
        this.SwitchState(target.ToString());
    }

    public void SwitchState(string targetStateName)
    {
        if (this.curStateName != targetStateName)
        {
            curState.DoBeforeLeaving();

            FsmState targetState = states.Find(t => t.stateName == targetStateName);

            curState = targetState;
            curStateName = targetState.stateName;

            targetState.DoBeforeEntering();
        }
    }
}

