﻿using UnityEngine;
using System.Collections;

public class PanelUnit
{
    public PANEL_ID panelId;
    //public PANEL_GROUP owenrGroup = PANEL_GROUP.none;
    

    public BaseScene sceneObj;
    public SceneId sceneId; //场景类型

    public GameObject mRoot;
    public string mResPath = string.Empty;
    public bool mResident;  //是否常驻内存
    public bool isVisable = false;

    public bool Create()
    {
        if (mRoot != null) return false;

        string loadPath = UI.prePath + "/" + mResPath;
        GameObject prefab = Resources.Load<GameObject>(loadPath);
        if (prefab == null) Debug.LogError("this ? " + loadPath);
        mRoot = NGUITools.AddChild(sceneObj.gameObject, prefab);
        mRoot.gameObject.SetActive(false);
        return true;
    }

    //游戏事件注册
    protected virtual void OnAddListener() { }

    //游戏事件注销
    protected virtual void OnRemoveListener() { }
    

    //////////////////////////////////////

    public virtual void Init() { }

    //窗口控制初始化
    public virtual void InitWidget() { }
    //窗口释放
    public virtual void RealseWidget() { }

    //类对象释放
    public virtual void Release() { }

    //显示初始化
    public virtual void OnEnable() { }

    public virtual void OnUpdate(float deltaTime, float unscaledDeltaTime) { }

    //隐藏
    public virtual void OnDisable() { }

    public void Show()
    {
        if (mRoot == null)
        {
            if (Create())
            {
                InitWidget();
            }
        }
        if (mRoot && mRoot.gameObject.activeSelf == false)
        {
            //Debug.Log(" mRoot.gameObject.activeSelf " + mRoot.gameObject.activeSelf + mRoot.transform + "SHOW");

            mRoot.gameObject.SetActive(true);
            OnEnable();
            OnAddListener();

            isVisable = true;
        }
    }

    public void Hide()
    {
        if (mRoot && mRoot.gameObject.activeSelf == true)
        {
            //Debug.Log(" mRoot.gameObject.activeSelf " + mRoot.gameObject.activeSelf + mRoot.transform + "Hide" + " mResident " + mResident);

            OnRemoveListener();
            OnDisable();
            if (mResident)
            {
                mRoot.gameObject.SetActive(false);
            }
            else
            {
                RealseWidget();
                Destroy();
            }

            isVisable = false;
        }
    }

    //预加载
    public void PreLoad()
    {
        if (mRoot == null)
        {
            if (Create())
            {
                InitWidget();
            }
        }
    }

    //延时删除
    public void DelayDestroy()
    {
        if (mRoot)
        {
            RealseWidget();
            Destroy();
        }
    }

    //销毁窗体
    protected void Destroy()
    {
        if (mRoot)
        {
            NGUITools.Destroy(mRoot.gameObject);
            mRoot = null;
        }
    }
}
